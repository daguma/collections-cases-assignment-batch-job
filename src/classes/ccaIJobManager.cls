public interface ccaIJobManager {

    Database.QueryLocator start(Database.BatchableContext BC, ccaJob job);
    void execute(Database.BatchableContext BC, List<Object> scope, ccaJob job);
    void execute(SchedulableContext sc, ccaJob job);
    void finish(Database.BatchableContext BC, ccaJob job);

}