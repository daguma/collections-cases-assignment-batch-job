/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaScenario.cls
 *
 * An entity class created to Map the loan__loan_account__c its Case and its Last Payment records into one object.
 *
 * Through the toCCALog() method, it maps itself into a ccaLog__c instance.
 *
 * </p>
 */
public class ccaScenario {

    public Id contractId, caseId, collectionsCaseOwnerId, caseOwnerId, caseOwnerNewId, assignedTo, caseCreated
            , previousCaseOwner;
    public Integer dpd;
    public Boolean updateCase = false, createCase = false, isLastPaymentReturned = false;
    public String type, analysisDescription, loadBalanceGroup, lastPaymentReversalReason, contractStatus
            , assignmentDescription, caseOwnerName, previousCaseOwnerName;

    public ccaScenario(loan__loan_account__c aContract, Case aCase, loan__Loan_Payment_Transaction__c aPayment) {
        if (aContract != null) {
            this.contractId = aContract.Id;
            this.contractStatus = aContract.loan__loan_status__c;
            this.collectionsCaseOwnerId = aContract.Collection_Case_Owner__c;
            this.dpd = Integer.valueOf(aContract.Overdue_Days__c);
            this.type = aContract.DMC__c != null
                    ? ccaUtil.SCENARIO_TYPE_DMC
                    : 'Point Of Need'.equalsIgnoreCase(aContract.Product_Name__c)
                            ? ccaUtil.SCENARIO_TYPE_PON
                            : ccaUtil.SCENARIO_TYPE_DTC;

            if (aCase != null) {
                this.caseId = aCase.Id;
                this.caseOwnerId = aCase.Owner.Id;
                this.caseOwnerName = aCase.Owner.Name;
                this.previousCaseOwner = aCase.PreviousOwner__c;
                this.previousCaseOwnerName = aCase.PreviousOwner__r.name;
            }

            if (aPayment != null) {
                this.lastPaymentReversalReason = String.isEmpty(aPayment.loan__Reversal_Reason__c) ? '' : aPayment.loan__Reversal_Reason__c;
                this.isLastPaymentReturned = this.lastPaymentReversalReason.contains('R01')
                        || this.lastPaymentReversalReason.contains('R02')
                        || this.lastPaymentReversalReason.contains('R03')
                        || this.lastPaymentReversalReason.contains('R08')
                        || this.lastPaymentReversalReason.contains('R16');
            }
        }
    }

    public void setAnalysisFields(String analysisDescription, Boolean updateCase) {
        setAnalysisFields(analysisDescription, updateCase, false, null, null, null);
    }

    public void setAnalysisFields(String analysisDescription, Boolean updateCase, Boolean createCase) {
        setAnalysisFields(analysisDescription, updateCase, createCase, null, null, null);
    }

    public void setAnalysisFields(String analysisDescription, Boolean updateCase, Boolean createCase, Id caseOwnerNewId) {
        setAnalysisFields(analysisDescription, updateCase, createCase, caseOwnerNewId, null, null);
    }

    public void setAnalysisFields(String analysisDescription, Boolean updateCase, Boolean createCase, Id caseOwnerNewId, Id previousCaseOwner) {
        setAnalysisFields(analysisDescription, updateCase, createCase, caseOwnerNewId, previousCaseOwner, null);
    }

    public void setAnalysisFields(String analysisDescription, Boolean updateCase, Boolean createCase, Id caseOwnerNewId, Id previousCaseOwner, String loadBalanceGroup) {
        this.analysisDescription = analysisDescription;
        this.updateCase = updateCase;
        this.createCase = createCase;
        this.caseOwnerNewId = caseOwnerNewId;
        this.previousCaseOwner = previousCaseOwner;
        this.loadBalanceGroup = loadBalanceGroup;
    }

    public ccaLog__c toCCALog(Integer analyzeBatchNumber) {
        return new ccaLog__c(
                contract__c = this.contractId
                , contractStatus__c = this.contractStatus
                , dpd__c = this.dpd
                , caseOwner__c = this.caseOwnerId
                , collectionsCaseOwner__c = this.collectionsCaseOwnerId
                , Case__c = this.caseId
                , ScenarioAnalysisDescription__c = this.analysisDescription
                , ScenarioAssignmentDescription__c = this.assignmentDescription
                , lastPaymentReversalReason__c = this.lastPaymentReversalReason
                , caseOwnerNew__c = this.caseOwnerNewId
                , isCaseCreationNeeded__c = this.createCase
                , isCaseUpdateNeeded__c = this.updateCase
                , LoadBalanceGroup__c = this.loadBalanceGroup
                , type__c = this.type
                , AssignedTo__c = this.assignedTo
                , CaseCreated__c = this.caseCreated
                , BatchNumberAnalyze__c = String.valueOf(analyzeBatchNumber)
        );
    }
}