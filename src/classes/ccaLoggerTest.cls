@isTest
public class ccaLoggerTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                DELETE_scope__c = 1500,
                ANALYZE_scope__c = 350,
                ASSIGN_scope__c = 100,
                REPORT_scope__c = 1500,
                Attach_csv_to_Audit_email__c = true,
                Include_DEBUG_info_in_Audit_email__c = true
        );
        insert cuSettings;
    }

    @isTest static void validate_constructor() {
        Test.startTest();
        system.assertNotEquals(null, ccaLogger.getInstance());
        Test.stopTest();
    }

    @isTest static void validate_log_event() {
        Test.startTest();
        system.assert(ccaLogger.getInstance().logList.isEmpty());
        ccaLogger.getInstance().logEvent('An important event');
        system.assert(!ccaLogger.getInstance().logList.isEmpty());
        Test.stopTest();
    }

    @isTest static void validate_log_Debug_Info() {
        Test.startTest();
        system.assert(ccaLogger.getInstance().logList.isEmpty());
        ccaLogger.getInstance().logDebugInfo('An important event');
        system.assert(!ccaLogger.getInstance().logList.isEmpty());
        Test.stopTest();
    }

    @isTest static void validate_get_log_list() {
        Test.startTest();
        system.assert(ccaLogger.getInstance().getLogList().isEmpty());
        ccaLogger.getInstance().logEvent('An important event');
        system.assert(!ccaLogger.getInstance().getLogList().isEmpty());
        Test.stopTest();
    }

}