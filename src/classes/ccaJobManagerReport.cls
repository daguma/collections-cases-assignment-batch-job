public class ccaJobManagerReport implements ccaIJobManager {

    public static Database.QueryLocator start(Database.BatchableContext BC, ccaJob job) {
        return Database.getQueryLocator(
                'SELECT Id, AssignedTo__r.name, CaseCreated__r.CaseNumber, CaseOwnerNew__r.name, CaseOwner__r.name'
                        + ' , Case__r.CaseNumber, Case__r.PreviousOwner__r.name, Case__r.ccaLoadBalanceGroup__c'
                        + ' , Contract__r.name, ContractStatus__c, Contract__r.DMC__c, Contract__r.Product_Name__c'
                        + ' , CollectionsCaseOwner__r.name, Dpd__c, LastPaymentReversalReason__c'
                        + ' , LoadBalanceGroup__c, ScenarioAnalysisDescription__c, ScenarioAssignmentDescription__c'
                        + ' , Type__c, isCaseCreationNeeded__c, isCaseUpdateNeeded__c'
                        + ' , BatchNumberAnalyze__c, BatchNumberAssign__c'
                        + ' FROM ccaLog__c'
        );
    }

    public static void execute(Database.BatchableContext BC, List<Object> scope, ccaJob job) {

        //Use the already loaded working context in every batch
        ccaJobContext.shareInstance(job.jobContext);

        //Log the Limits.getHeapSize() for analysis purposes
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerReport - execute - Heap Size Limit is 12MB - Current use: ' + Limits.getHeapSize());

        //Add the ccaLog__c records to the report that will be sent by email at the "Finish" stage of this job
        //If custom setting allows
        if (ccaJobContext.getInstance().getCustomSetting().Attach_csv_to_Audit_email__c) {
            for (ccaLog__c log : (List<ccaLog__c>) scope) {
                job.getJobContext().getJobSummaryReport().addReportLine(log);
            }
        }
    }

    public static void execute(SchedulableContext sc, ccaJob job) {
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().REPORT_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().REPORT_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().REPORT_scope__c.intValue()
                        : 1000
        );
    }

    public static void finish(Database.BatchableContext BC, ccaJob job) {

        //Share the working context with each and every one of the batches
        ccaJobContext.shareInstance(job.getJobContext());

        //Email the report
        emailLog(null);
    }

    @TestVisible private static void sendEmail(String subject, String body, String[] toAddresses, List<Messaging.EmailFileAttachment> attachmentList) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        if (attachmentList != null && !attachmentList.isEmpty()) {
            email.setFileAttachments(attachmentList);
        }
        email.setHtmlBody(body);
        email.setSaveAsActivity(false);

        try {
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        email
                });
            }
        } catch (Exception e) {
            throw new ccaJobManagerReportException('ccaJobManagerReport - sendEmail - Exception: ' + e.getMessage());
        }

    }

    public static void emailLog(String[] toAddresses) {

        //Set up "reportDate" to add it in the file names
        Date reportDate = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().day());

        //Set up the UsersStructure report
        Messaging.EmailFileAttachment usersStructureAttachment = new Messaging.EmailFileAttachment();
        blob usersStructureBlob = Blob.valueOf(JSON.serialize(ccaJobContext.getInstance().getUsersStructure()));
        string usersStructurename = 'CCA Users Structure ' + String.valueOf(Date.today()) + '.json';
        usersStructureAttachment.setFileName(usersStructurename);
        usersStructureAttachment.setBody(usersStructureBlob);
        List<Messaging.EmailFileAttachment> attachmentsList = new List<Messaging.EmailFileAttachment>{
                usersStructureAttachment
        };

        //Set up the csv report
        if (ccaJobContext.getInstance().getCustomSetting().Attach_csv_to_Audit_email__c) {
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(ccaJobContext.getInstance().getJobSummaryReport().logReportToString());
            string csvname = 'Collections Cases Assignment ' + String.valueOf(Date.today()) + '.csv';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);
            attachmentsList.add(csvAttc);
        }

        //Set up the AssignmentStructure report
        Messaging.EmailFileAttachment assignmentStructureAttachment = new Messaging.EmailFileAttachment();
        blob assignmentStructureBlob = Blob.valueOf(ccaJobContext.getInstance().getAssignmentsStructure().getInitialAssignmentsStructure());
        string assignmentStructureName = 'CCA Assignments Structure ' + String.valueOf(Date.today()) + '.json';
        assignmentStructureAttachment.setFileName(assignmentStructureName);
        assignmentStructureAttachment.setBody(assignmentStructureBlob);
        attachmentsList.add(assignmentStructureAttachment);

        //Set up the Events Log report
        Messaging.EmailFileAttachment eventsAttachment = new Messaging.EmailFileAttachment();
        blob eventLogBlob = Blob.valueOf(JSON.serialize(ccaJobContext.getInstance().getLogger().getLogList()));
        string eventsName = 'CCA Events Log ' + String.valueOf(Date.today()) + '.json';
        eventsAttachment.setFileName(eventsName);
        eventsAttachment.setBody(eventLogBlob);
        attachmentsList.add(eventsAttachment);

        //Summary
        /*
        Messaging.EmailFileAttachment summaryAttachment = new Messaging.EmailFileAttachment();
        blob summaryBlob = Blob.valueOf(JSON.serialize(ccaJobContext.getInstance().getJobSummaryReport().toString()));
        string summaryName = 'CCA Summary ' + String.valueOf(Date.today()) + '.json';
        summaryAttachment.setFileName(summaryName);
        summaryAttachment.setBody(summaryBlob);
        attachmentsList.add(summaryAttachment);
         */

        //Main mail body
        String reportBody = '<br/>The Collections Cases Assignment process has finished running.';
        reportBody += '<br/><br/>Please find attached the CSV file report for today.';

        //Set up the addresses that will be used to send the email
        List<String> toAddressesList = new List<String>();
        if (toAddresses != null) {
            for (String toAddress : toAddresses) {
                if (toAddress.contains('@')) {
                    toAddressesList.add(toAddress);
                }
            }
        } else {
            String auditPublicGroupName = ccaJobContext.getInstance().getCustomSetting().Audit_public_group__c;
            if (String.isEmpty(auditPublicGroupName)) {
                throw new ccaJobManagerReportException('ccaJobManagerReport - emailLog - No Audit_public_group__c in ccaCustomSetting(cca)');
            }
            Id auditPublicGroupId = [SELECT Id FROM Group WHERE Name = :auditPublicGroupName LIMIT 1].Id;
            Map<Id, AggregateResult> usersToEmailLog = new Map<Id, AggregateResult>([SELECT UserOrGroupId Id FROM GroupMember WHERE GroupId = :auditPublicGroupId GROUP BY UserOrGroupId]);
            toAddresses = (List<String>) new List<Id>(usersToEmailLog.keySet());
        }

        //Send the email
        sendEmail('Collections Cases Assignment Audit Report ' + reportDate.format(), reportBody, toAddresses, attachmentsList);

    }

    public class ccaJobManagerReportException extends Exception {
    }
}