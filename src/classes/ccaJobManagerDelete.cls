public class ccaJobManagerDelete implements ccaIJobManager {

    public static Database.QueryLocator start(Database.BatchableContext BC, ccaJob job) {
        return Database.getQueryLocator('SELECT Id FROM ccaLog__c');
    }

    public static void execute(Database.BatchableContext BC, List<Object> scope, ccaJob job) {
        if (scope != null && !scope.isEmpty()) delete (List<ccaLog__c>) scope;
    }

    public static void execute(SchedulableContext sc, ccaJob job) {
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().DELETE_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().DELETE_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().DELETE_scope__c.intValue()
                        : 1500
        );
    }

    public static void finish(Database.BatchableContext BC, ccaJob job) {
        job.jobAction = ccaUtil.JOB_ACTION_ANALYZE;
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().ANALYZE_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().ANALYZE_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().ANALYZE_scope__c.intValue()
                        : 350
        );
    }
}