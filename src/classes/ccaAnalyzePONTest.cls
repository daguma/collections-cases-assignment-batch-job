@isTest
public class ccaAnalyzePONTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_is_cured() {
        Test.startTest();
        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaAnalyzePON.isCured(scenario);
        system.assertEquals('PON - Cured - Assign to HAL', scenario.analysisDescription);
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(Label.HAL_Id, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);

        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        scenario2.caseOwnerId = Label.HAL_Id;
        scenario2.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaAnalyzePON.isCured(scenario2);
        system.assertEquals('PON - Cured - Assign to HAL', scenario2.analysisDescription);
        system.assertEquals(false, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(Label.HAL_Id, scenario2.caseOwnerNewId);
        system.assertEquals(Label.HAL_Id, scenario2.previousCaseOwner);
        system.assertEquals(null, scenario2.loadBalanceGroup);

        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        scenario3.type = ccaUtil.SCENARIO_TYPE_PON;
        scenario3.caseOwnerId = [SELECT Id FROM User WHERE name != 'HAL' LIMIT 1].Id;
        ccaAnalyzePON.isCured(scenario3);
        system.assertEquals('PON - Cured - Assign to HAL', scenario3.analysisDescription);
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(Label.HAL_Id, scenario3.caseOwnerNewId);
        system.assertEquals(scenario3.caseOwnerId, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_returned_payment() {
        Test.startTest();
        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        Group ccaPON1 = [SELECT Id FROM Group WHERE developername = 'PON_1_15'];
        Id ccaPON1CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON1.Id LIMIT 1].UserOrGroupId;
        scenario.caseOwnerId = ccaPON1CollectorId;
        scenario.caseOwnerName = [SELECT name FROM User WHERE Id = :ccaPON1CollectorId LIMIT 1].name;
        scenario.dpd = 1;
        ccaAnalyzePON.isReturnedPayment(scenario);
        system.debug(scenario);
        system.assert(scenario.analysisDescription.contains('PON - Return payments - Already correctly assigned to'));
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_returned_payment_2() {
        Test.startTest();
        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        scenario2.type = ccaUtil.SCENARIO_TYPE_PON;
        Group ccaPON1_2 = [SELECT Id FROM Group WHERE developername = 'PON_1_15'];
        Id ccaPON1CollectorId_2 = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON1_2.Id LIMIT 1].UserOrGroupId;
        scenario2.caseOwnerId = ccaPON1CollectorId_2;
        scenario2.caseOwnerName = [SELECT name FROM User WHERE Id = :ccaPON1CollectorId_2 LIMIT 1].name;
        scenario2.dpd = 16;
        ccaAnalyzePON.isReturnedPayment(scenario2);
        system.debug(scenario2);
        system.assert(scenario2.analysisDescription.contains('PON - Return payments - Assign to corresponding DPD Team'));
        system.assertEquals(true, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(null, scenario2.previousCaseOwner);
        system.assertEquals('ccaPON2', scenario2.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_returned_payment_3() {
        Test.startTest();
        Case aCase3 = new Case();
        insert aCase3;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase3, null);
        scenario3.type = ccaUtil.SCENARIO_TYPE_PON;
        Group ccaPON1_3 = [SELECT Id FROM Group WHERE developername = 'PON_1_15'];
        Id ccaPON1CollectorId_3 = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON1_3.Id LIMIT 1].UserOrGroupId;
        scenario3.caseOwnerId = ccaPON1CollectorId_3;
        scenario3.caseOwnerName = [SELECT name FROM User WHERE Id = :ccaPON1CollectorId_3 LIMIT 1].name;
        scenario3.dpd = 16;
        Group ccaPON2 = [SELECT Id FROM Group WHERE developername = 'PON_16_60'];
        Id ccaPON1CollectorId_3_previous = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON2.Id LIMIT 1].UserOrGroupId;
        scenario3.previousCaseOwner = ccaPON1CollectorId_3_previous;
        ccaAnalyzePON.isReturnedPayment(scenario3);
        system.debug(scenario3);
        system.assert(scenario3.analysisDescription.contains('PON - Return payments - Assign to Previous Owner'));
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(ccaPON1CollectorId_3_previous, scenario3.caseOwnerNewId);
        system.assertEquals(ccaPON1CollectorId_3, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_dpd() {
        Test.startTest();

        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.dpd = 1;
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaAnalyzePON.isDpdTeam(scenario);
        system.assert(scenario.analysisDescription.contains('PON - Dpd - Case does not exist - Create case - Load balance in'));
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaPON1', scenario.loadBalanceGroup);

        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        scenario2.type = ccaUtil.SCENARIO_TYPE_PON;
        Group ccaPON1 = [SELECT Id FROM Group WHERE developername = 'PON_1_15'];
        Id ccaPON1CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON1.Id LIMIT 1].UserOrGroupId;
        scenario2.dpd = 1;
        scenario2.caseOwnerId = ccaPON1CollectorId;
        ccaAnalyzePON.isDpdTeam(scenario2);
        system.assert(scenario2.analysisDescription.contains('PON - Dpd - It is correctly assigned to'));
        system.assertEquals(false, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(null, scenario2.previousCaseOwner);
        system.assertEquals(null, scenario2.loadBalanceGroup);

        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        scenario3.type = ccaUtil.SCENARIO_TYPE_PON;
        Group ccaPON2 = [SELECT Id FROM Group WHERE developername = 'PON_16_60'];
        Id ccaPON2CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON2.Id LIMIT 1].UserOrGroupId;
        scenario3.dpd = 1;
        scenario3.caseOwnerId = ccaPON2CollectorId;
        ccaAnalyzePON.isDpdTeam(scenario3);
        system.assert(scenario3.analysisDescription.contains('PON - Dpd - Reassign - load balance in'));
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(null, scenario3.caseOwnerNewId);
        system.assertEquals(ccaPON2CollectorId, scenario3.previousCaseOwner);
        system.assertEquals('ccaPON1', scenario3.loadBalanceGroup);

        Test.stopTest();
    }

    static testMethod void validate_analyze() {
        Test.startTest();
        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.dpd = 0;
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaAnalyzePON.analyze(scenario);
        system.assertEquals('PON - Cured - Assign to HAL', scenario.analysisDescription);

        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario2.type = ccaUtil.SCENARIO_TYPE_PON;
        scenario2.dpd = 1;
        scenario2.isLastPaymentReturned = true;
        ccaAnalyzePON.analyze(scenario2);
        system.assert(scenario2.analysisDescription.contains('PON - Return payments'));

        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario3.dpd = 1;
        scenario3.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaAnalyzePON.analyze(scenario3);
        system.assert(scenario3.analysisDescription.contains('PON - Dpd'));

        ccaScenario scenario4 = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario4.dpd = 1;
        scenario4.type = ccaUtil.SCENARIO_TYPE_PON;
        scenario4.contractStatus = 'Closed- Written Off';
        ccaAnalyzePON.analyze(scenario4);
        system.assert(scenario4.analysisDescription.contains('PON Default'));
        Test.stopTest();
    }

    static testMethod void validate_is_default() {
        Test.startTest();

        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.dpd = 1;
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        scenario.contractStatus = 'Closed- Written Off';
        ccaAnalyzePON.isDefault(scenario);
        system.assert(scenario.analysisDescription.contains('Default'));
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaPONDefault', scenario.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_default_2() {
        Test.startTest();

        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        scenario2.type = ccaUtil.SCENARIO_TYPE_PON;
        scenario2.contractStatus = 'Closed- Written Off';
        Group ccaPON1 = [SELECT Id FROM Group WHERE developername = 'PON_1_15'];
        Id ccaPON1CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON1.Id LIMIT 1].UserOrGroupId;
        scenario2.dpd = 1;
        scenario2.caseOwnerId = ccaPON1CollectorId;
        ccaAnalyzePON.isDefault(scenario2);
        system.assert(scenario2.analysisDescription.contains('Default'));
        system.assertEquals(true, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(ccaPON1CollectorId, scenario2.previousCaseOwner);
        system.assertEquals('ccaPONDefault', scenario2.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_default_3() {
        Test.startTest();

        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        scenario3.type = ccaUtil.SCENARIO_TYPE_PON;
        scenario3.contractStatus = 'Closed- Written Off';
        Group ccaPON2 = [SELECT Id FROM Group WHERE developername = 'PON_Default'];
        Id ccaPON2CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaPON2.Id LIMIT 1].UserOrGroupId;
        scenario3.dpd = 1;
        scenario3.caseOwnerId = ccaPON2CollectorId;
        ccaAnalyzePON.isDefault(scenario3);
        system.assert(scenario3.analysisDescription.contains('Is PON Default - Already assigned to'));
        system.assertEquals(false, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(null, scenario3.caseOwnerNewId);
        system.assertEquals(null, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);

        Test.stopTest();
    }

}