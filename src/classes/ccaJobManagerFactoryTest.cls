@isTest
public class ccaJobManagerFactoryTest {

    @isTest static void validate_get_job_manager() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobManagerFactory.getJobManager(ccaUtil.JOB_ACTION_DELETE));
        system.assert(ccaJobManagerFactory.getJobManager(ccaUtil.JOB_ACTION_DELETE) instanceof ccaJobManagerDelete);
        system.assert(ccaJobManagerFactory.getJobManager(ccaUtil.JOB_ACTION_ANALYZE) instanceof ccaJobManagerAnalyze);
        system.assert(ccaJobManagerFactory.getJobManager(ccaUtil.JOB_ACTION_ASSIGN) instanceof ccaJobManagerAssign);
        system.assert(ccaJobManagerFactory.getJobManager(ccaUtil.JOB_ACTION_REPORT) instanceof ccaJobManagerReport);

        try {
            ccaJobManagerFactory.getJobManager(null);
        } catch (Exception e) {
            system.assertEquals('ccaJobManagerFactory - Job action has to be provided', e.getMessage());
        }

        try {
            ccaJobManagerFactory.getJobManager('');
        } catch (Exception e) {
            system.assertEquals('ccaJobManagerFactory - Job action has to be provided', e.getMessage());
        }

        try {
            ccaJobManagerFactory.getJobManager('Some other job action');
        } catch (Exception e) {
            system.assertEquals('ccaJobManagerFactory - Provided job action unrecognized', e.getMessage());
        }

        Test.stopTest();
    }

}