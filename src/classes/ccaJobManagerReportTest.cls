@isTest
public class ccaJobManagerReportTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                Attach_csv_to_Audit_email__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_start() {
        Test.startTest();

        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_REPORT);
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        //job.execute(BC, logList);
        //job.finish(BC);
        system.assert(logList.isEmpty());

        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(Contract__c = contract.Id);
        insert log;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(!logList.isEmpty());
        Test.stopTest();
    }

    static testMethod void validate_execute() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_REPORT);
        system.assert(job.getJobContext().getJobSummaryReport().logReport.isEmpty());
        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(Contract__c = contract.Id);
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);
        system.assert(!job.getJobContext().getJobSummaryReport().logReport.isEmpty());
        Test.stopTest();
    }

    static testMethod void validate_execute_schedule() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_REPORT);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_send_email() {
        Test.startTest();
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf('1');
        string csvname = 'Test';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        List<Messaging.EmailFileAttachment> attachmentsList = new List<Messaging.EmailFileAttachment>{
                csvAttc
        };
        ccaJobManagerReport.sendEmail('Subject', 'Body', new List<String>{
                'dgutierrez@fuzionsoft.net'
        }, attachmentsList);
        Test.stopTest();
    }

    static testMethod void validate_finish() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_REPORT);
        ccaJobContext.getInstance().setUpAssignmentsStructure();
        ccaJobManagerReport.finish(null, job);
        Test.stopTest();
    }

    static testMethod void validate_email_log() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_REPORT);
        ccaJobContext.getInstance().setUpAssignmentsStructure();
        ccaJobManagerReport.emailLog(new List<String>{
                'dgutierrez@fuzionsoft.net'
        });
        Test.stopTest();
    }

}