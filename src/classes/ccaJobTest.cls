@isTest
public class ccaJobTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_constructor_empty() {
        Test.startTest();
        ccaJob job = new ccaJob();
        system.assertEquals(ccaUtil.JOB_ACTION_DELETE, job.jobAction);
        system.assertEquals(true, job.callNextAction);
        system.assertEquals(ccaJobContext.getInstance(), job.jobContext);
        Test.stopTest();
    }

    static testMethod void validate_constructor_one_arg() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        system.assertEquals(ccaUtil.JOB_ACTION_ANALYZE, job.jobAction);
        system.assertEquals(false, job.callNextAction);
        system.assertEquals(ccaJobContext.getInstance(), job.jobContext);
        Test.stopTest();
    }

    static testMethod void validate_get_job_context() {
        Test.startTest();
        ccaJob job = new ccaJob();
        system.assertEquals(ccaJobContext.getInstance(), job.getJobContext());
        Test.stopTest();
    }

    static testMethod void validate_init_job_context() {
        Test.startTest();
        ccaJob job = new ccaJob();
        job.initJobContext();
        system.assertNotEquals(null, job.getJobContext());
        Test.stopTest();
    }


    static testMethod void validate_start() {
        Test.startTest();
        ccaJob job = new ccaJob();
        Database.BatchableContext BC;
        Database.QueryLocator QL = job.start(bc);
        system.assertNotEquals(null, QL);
        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable() {
        Test.startTest();
        ccaJob job = new ccaJob();
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }


    static testMethod void validate_finish() {
        Test.startTest();
        ccaJob job = new ccaJob();
        job.finish(null);
        system.assertEquals(ccaUtil.JOB_ACTION_ANALYZE, job.jobAction);
        Test.stopTest();
    }

    static testMethod void validate_execute() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(logList.isEmpty());

        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(Contract__c = contract.Id);
        insert log;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(!logList.isEmpty());
        job.execute(BC, logList);

        logList.clear();
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(logList.isEmpty());
        system.assertEquals(1, job.batchNumber);
        Test.stopTest();
    }

}