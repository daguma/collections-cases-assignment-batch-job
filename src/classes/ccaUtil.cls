public class ccaUtil {

    //All the job actions
    public static String JOB_ACTION_DELETE = 'DELETE';
    public static String JOB_ACTION_ANALYZE = 'ANALYZE';
    public static String JOB_ACTION_ASSIGN = 'ASSIGN';
    public static String JOB_ACTION_REPORT = 'REPORT';

    //Match job action with job manager class
    public static Map<String, String> JOB_MANAGERS_MAP {
        get {
            if (JOB_MANAGERS_MAP == null) {
                JOB_MANAGERS_MAP = new Map<String, String>{
                        JOB_ACTION_DELETE => 'ccaJobManagerDelete',
                        JOB_ACTION_ANALYZE => 'ccaJobManagerAnalyze',
                        JOB_ACTION_ASSIGN => 'ccaJobManagerAssign',
                        JOB_ACTION_REPORT => 'ccaJobManagerReport'
                };
            }
            return JOB_MANAGERS_MAP;
        }
        set;
    }

    //Types of scenarios
    public static String SCENARIO_TYPE_DTC = 'ccaDTC';
    public static String SCENARIO_TYPE_PON = 'ccaPON';
    public static String SCENARIO_TYPE_DMC = 'ccaDMC';

    //Match scenario type with scenario analyzer class
    public static Map<String, String> SCENARIO_ANALYZERS_MAP {
        get {
            if (SCENARIO_ANALYZERS_MAP == null) {
                SCENARIO_ANALYZERS_MAP = new Map<String, String>{
                        SCENARIO_TYPE_DTC => 'ccaAnalyzeDTC',
                        SCENARIO_TYPE_PON => 'ccaAnalyzePON',
                        SCENARIO_TYPE_DMC => 'ccaAnalyzeDMC'
                };
            }
            return SCENARIO_ANALYZERS_MAP;
        }
        set;
    }
}