@isTest
public class ccaAnalyzeDTCTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_is_cured() {
        Test.startTest();
        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        ccaAnalyzeDTC.isCured(scenario);
        system.assertEquals('DTC - Cured - Assign to HAL', scenario.analysisDescription);
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(Label.HAL_Id, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);

        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        scenario2.caseOwnerId = Label.HAL_Id;
        ccaAnalyzeDTC.isCured(scenario2);
        system.assertEquals('DTC - Cured - Assign to HAL', scenario2.analysisDescription);
        system.assertEquals(false, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(Label.HAL_Id, scenario2.caseOwnerNewId);
        system.assertEquals(Label.HAL_Id, scenario2.previousCaseOwner);
        system.assertEquals(null, scenario2.loadBalanceGroup);

        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        scenario3.caseOwnerId = [SELECT Id FROM User WHERE name != 'HAL' LIMIT 1].Id;
        ccaAnalyzeDTC.isCured(scenario3);
        system.assertEquals('DTC - Cured - Assign to HAL', scenario3.analysisDescription);
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(Label.HAL_Id, scenario3.caseOwnerNewId);
        system.assertEquals(scenario3.caseOwnerId, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_returned_payment() {
        Test.startTest();
        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        Group ccaDTC1 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Id ccaDTC1CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC1.Id LIMIT 1].UserOrGroupId;
        scenario.caseOwnerId = ccaDTC1CollectorId;
        scenario.caseOwnerName = [SELECT name FROM User WHERE Id = :ccaDTC1CollectorId LIMIT 1].name;
        scenario.dpd = 1;
        ccaAnalyzeDTC.isReturnedPayment(scenario);
        system.debug(scenario);
        system.assert(scenario.analysisDescription.contains('DTC - Return payments - Already correctly assigned to'));
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_returned_payment_2() {
        Test.startTest();
        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        Group ccaDTC1_2 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Id ccaDTC1CollectorId_2 = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC1_2.Id LIMIT 1].UserOrGroupId;
        scenario2.caseOwnerId = ccaDTC1CollectorId_2;
        scenario2.caseOwnerName = [SELECT name FROM User WHERE Id = :ccaDTC1CollectorId_2 LIMIT 1].name;
        scenario2.dpd = 16;
        ccaAnalyzeDTC.isReturnedPayment(scenario2);
        system.debug(scenario2);
        system.assert(scenario2.analysisDescription.contains('DTC - Return payments - Assign to corresponding DPD Team'));
        system.assertEquals(true, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(null, scenario2.previousCaseOwner);
        system.assertEquals('ccaDTC2', scenario2.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_returned_payment_3() {
        Test.startTest();
        Case aCase3 = new Case();
        insert aCase3;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase3, null);
        Group ccaDTC1_3 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Id ccaDTC1CollectorId_3 = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC1_3.Id LIMIT 1].UserOrGroupId;
        scenario3.caseOwnerId = ccaDTC1CollectorId_3;
        scenario3.caseOwnerName = [SELECT name FROM User WHERE Id = :ccaDTC1CollectorId_3 LIMIT 1].name;
        scenario3.dpd = 16;
        Group ccaDTC2 = [SELECT Id FROM Group WHERE developername = 'DTC_16_60'];
        Id ccaDTC1CollectorId_3_previous = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC2.Id LIMIT 1].UserOrGroupId;
        scenario3.previousCaseOwner = ccaDTC1CollectorId_3_previous;
        ccaAnalyzeDTC.isReturnedPayment(scenario3);
        system.debug(scenario3);
        system.assert(scenario3.analysisDescription.contains('DTC - Return payments - Assign to Previous Owner'));
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(ccaDTC1CollectorId_3_previous, scenario3.caseOwnerNewId);
        system.assertEquals(ccaDTC1CollectorId_3, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_dpd() {
        Test.startTest();

        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.dpd = 1;
        ccaAnalyzeDTC.isDpdTeam(scenario);
        system.assert(scenario.analysisDescription.contains('DTC - Dpd - Case does not exist - Create case - Load balance in'));
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaDTC1', scenario.loadBalanceGroup);

        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        Group ccaDTC1 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Id ccaDTC1CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC1.Id LIMIT 1].UserOrGroupId;
        scenario2.dpd = 1;
        scenario2.caseOwnerId = ccaDTC1CollectorId;
        ccaAnalyzeDTC.isDpdTeam(scenario2);
        system.assert(scenario2.analysisDescription.contains('DTC - Dpd - It is correctly assigned to'));
        system.assertEquals(false, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(null, scenario2.previousCaseOwner);
        system.assertEquals(null, scenario2.loadBalanceGroup);

        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        Group ccaDTC2 = [SELECT Id FROM Group WHERE developername = 'DTC_16_60'];
        Id ccaDTC2CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC2.Id LIMIT 1].UserOrGroupId;
        scenario3.dpd = 1;
        scenario3.caseOwnerId = ccaDTC2CollectorId;
        ccaAnalyzeDTC.isDpdTeam(scenario3);
        system.assert(scenario3.analysisDescription.contains('DTC - Dpd - Reassign - load balance in'));
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(null, scenario3.caseOwnerNewId);
        system.assertEquals(ccaDTC2CollectorId, scenario3.previousCaseOwner);
        system.assertEquals('ccaDTC1', scenario3.loadBalanceGroup);

        Test.stopTest();
    }

    static testMethod void validate_analyze() {
        Test.startTest();
        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.dpd = 0;
        ccaAnalyzeDTC.analyze(scenario);
        system.assertEquals('DTC - Cured - Assign to HAL', scenario.analysisDescription);

        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario2.dpd = 1;
        scenario2.isLastPaymentReturned = true;
        ccaAnalyzeDTC.analyze(scenario2);
        system.assert(scenario2.analysisDescription.contains('DTC - Return payments'));

        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario3.dpd = 1;
        ccaAnalyzeDTC.analyze(scenario3);
        system.assert(scenario3.analysisDescription.contains('DTC - Dpd'));

        ccaScenario scenario4 = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario4.dpd = 1;
        scenario4.type = ccaUtil.SCENARIO_TYPE_DTC;
        scenario4.contractStatus = 'Closed- Written Off';
        ccaAnalyzeDTC.analyze(scenario4);
        system.assert(scenario4.analysisDescription.contains('DTC Default'));
        Test.stopTest();
    }

    static testMethod void validate_is_default() {
        Test.startTest();

        ccaScenario scenario = new ccaScenario(LibraryTest.createContractTH(), null, null);
        scenario.dpd = 1;
        scenario.type = ccaUtil.SCENARIO_TYPE_DTC;
        scenario.contractStatus = 'Closed- Written Off';
        ccaAnalyzeDTC.isDefault(scenario);
        system.assert(scenario.analysisDescription.contains('Default'));
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaDTCDefault', scenario.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_default_2() {
        Test.startTest();

        Case aCase = new Case();
        insert aCase;
        ccaScenario scenario2 = new ccaScenario(LibraryTest.createContractTH(), aCase, null);
        scenario2.type = ccaUtil.SCENARIO_TYPE_DTC;
        scenario2.contractStatus = 'Closed- Written Off';
        Group ccaDTC1 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Id ccaDTC1CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC1.Id LIMIT 1].UserOrGroupId;
        scenario2.dpd = 1;
        scenario2.caseOwnerId = ccaDTC1CollectorId;
        ccaAnalyzeDTC.isDefault(scenario2);
        system.assert(scenario2.analysisDescription.contains('Default'));
        system.assertEquals(true, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(ccaDTC1CollectorId, scenario2.previousCaseOwner);
        system.assertEquals('ccaDTCDefault', scenario2.loadBalanceGroup);
        Test.stopTest();
    }

    static testMethod void validate_is_default_3() {
        Test.startTest();

        Case aCase2 = new Case();
        insert aCase2;
        ccaScenario scenario3 = new ccaScenario(LibraryTest.createContractTH(), aCase2, null);
        scenario3.type = ccaUtil.SCENARIO_TYPE_DTC;
        scenario3.contractStatus = 'Closed- Written Off';
        Group ccaDTC2 = [SELECT Id FROM Group WHERE developername = 'DTC_Default'];
        Id ccaDTC2CollectorId = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :ccaDTC2.Id LIMIT 1].UserOrGroupId;
        scenario3.dpd = 1;
        scenario3.caseOwnerId = ccaDTC2CollectorId;
        ccaAnalyzeDTC.isDefault(scenario3);
        system.assert(scenario3.analysisDescription.contains('Is DTC Default - Already assigned to'));
        system.assertEquals(false, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(null, scenario3.caseOwnerNewId);
        system.assertEquals(null, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);

        Test.stopTest();
    }

}