/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 * The process is designed to run four consecutive jobs as quick as possible in order to assign Collections Cases.
 * These jobs are selected by the ccaJobManagerFactory.cls.
 *
 *  1. The first job: DELETE. ccaJobManagerDelete.cls
 *   - Makes sure the ccaLog__c object is empty for a new run.
 *
 *  2. The second job: ANALYZE. ccaJobManagerAnalyze.cls
 *   - Builds a users structure (ccaUsersStructure.cls) from public groups started by Main_public_group__c
 *   in the Custom Setting: Collections Cases Assignment. This is done once and will be common for all the batches.
 *   - Gathers the loan__loan_account__c, Case and loan__Loan_Payment_Transaction__c records and
 *   builds a ccaScenario.cls for each contract (ccaScenarioAnalyzerFactory.cls).
 *   - Compares the scenario against the business logic for each type (ccaAnalyzePON.cls, ccaAnalyzeDTC.cls, ccaAnalyzeDMC.cls).
 *   - Writes the analysis results to ccaLog__c. Each record in ccaLog__c represents a scenario (contract).
 *
 *  3. The third job: ASSIGN. ccaJobManagerAssign.cls
 *   - Sets up the Assignments Structure. This is done once and will be common for all the batches.
 *   - Selects ALL the ccaLog__c records.
 *   - Builds the ccaAssignmentStructure.cls according to the stats collected.
 *   - Gets a new Collector for the Case from a load balancing algorithm
 *   (ccaLoadBalancingEntity.cls and ccaLoadBalancingAlgorithm.cls).
 *   - Updates/Creates the Case and Updates loan__loan_account__c records.
 *   - Updates the ccaLog__c with the assignment results.
 *
 *  4. The fourth job: REPORT. ccaJobManagerReport.cls
 *   - Selects ALL the ccaLog__c records.
 *   - Builds a List with useful logging information for each scenario.
 *   - Composes an email with file attachments: csv with all scenarios logging info, ccaUsersStructure,
 *   ccaAssignmentsStructure and Events.
 *   - Sends the email to the members in the Audit_public_group__c of the Custom Setting: Collections Cases Assignment.
 *
 *   From Anonymous:
 *   To run the full process: database.executebatch(new ccaJob(), 200);
 *   To run ONLY a specific ACTION: database.executebatch(new ccaJob('DELETE'), 200);
 *
 * </p>
 */
global class ccaJob implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    global String jobAction;
    global Boolean callNextAction;
    global ccaJobContext jobContext;
    global Integer batchNumber = 0;

    global ccaJob() {
        this.jobAction = ccaUtil.JOB_ACTION_DELETE;
        this.callNextAction = true;
        initJobContext();
    }
    global ccaJob(String action) {
        this.jobAction = action;
        this.callNextAction = false;
        initJobContext();
    }

    public ccaJobContext getJobContext() {
        return this.jobContext;
    }

    public void initJobContext() {
        this.jobContext = ccaJobContext.getInstance();
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return ccaJobManagerFactory.getJobManager(this.jobAction).start(BC, this);
    }

    global void execute(Database.BatchableContext BC, List<Object> scope) {
        this.batchNumber = this.batchNumber + 1;
        ccaJobManagerFactory.getJobManager(this.jobAction).execute(BC, scope, this);
    }

    global void execute(SchedulableContext sc) {
        ccaJobManagerFactory.getJobManager(this.jobAction).execute(sc, this);
    }

    global void finish(Database.BatchableContext BC) {
        if (this.callNextAction) {
            ccaJobManagerFactory.getJobManager(this.jobAction).finish(BC, this);
        }
    }

}