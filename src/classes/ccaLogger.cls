/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaLogger.cls
 *
 * Initialized once at the start of the ANALYZE job, it keeps a log of events during the execution of all the jobs.
 *
 * </p>
 */
public class ccaLogger {

    private static ccaLogger instance = null;

    public List<String> logList { get; private set; }

    private ccaLogger() {
        this.logList = new List<String>();
    }

    public static ccaLogger getInstance() {
        if (instance == null) instance = new ccaLogger();
        return instance;
    }

    public void logEvent(String eventDescription) {
        this.logList.add(eventDescription);
    }

    public void logDebugInfo(String eventDescription) {
        //If custom setting allows
        if (ccaJobContext.getInstance().getCustomSetting().Include_DEBUG_info_in_Audit_email__c) {
            this.logList.add(eventDescription);
        }
    }

    public List<String> getLogList() {
        return this.logList;
    }

}