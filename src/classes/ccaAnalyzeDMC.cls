public class ccaAnalyzeDMC implements ccaIScenarioAnalyzer {

    public static ccaScenario analyze(ccaScenario scenario) {
        if ('Closed- Written Off, Active - Bad Standing, Active - Good Standing'.containsIgnoreCase(scenario.contractStatus)) {
            isDMCTeam(scenario);
        } else if ('Closed - Settled Off, Closed - Sold Off'.containsIgnoreCase(scenario.contractStatus)) {
            isDefault(scenario);
        }
        return scenario;
    }

    public static void isDMCTeam(ccaScenario scenario) {
        Boolean isCaseUpdateNeeded = scenario.caseId == null
                ? false
                : scenario.caseOwnerId == null
                        ? true
                        : !ccaJobContext.getInstance().getUsersStructure()
                                .checkUserBelongsInDpdLevel(scenario.type, scenario.dpd, scenario.caseOwnerId);
        scenario.setAnalysisFields(
                'Is DMC Team' + (
                        isCaseUpdateNeeded
                                ? ' - Update case'
                                : scenario.caseId == null
                                ? ' - Create case'
                                : ccaJobContext.getInstance().getUsersStructure().checkUserBelongsInDpdLevel(scenario.type, scenario.dpd, scenario.caseOwnerId)
                                        ? ' - Already assigned to ' + scenario.caseOwnerName + ' in group: ccaDMC1' : '' //There is only one DMC team for now and getGroupNameByDpd does not work because DMC has no dpdMax limit
                        //loan__loan_account__r.Overdue_Days__c --> It keeps adding one day every day, regardless of the status of the contract.
                )
                , isCaseUpdateNeeded
                , scenario.caseId == null
                , null
                , isCaseUpdateNeeded ? scenario.caseOwnerId : null
                , isCaseUpdateNeeded || scenario.caseId == null ? 'ccaDMC1' : '' //There is only one DMC team for now, and getGroupNameByDpd does not work because DMC has no dpdMax limit
                //loan__loan_account__r.Overdue_Days__c --> It keeps adding one day every day, regardless of the status of the contract.
        );
    }

    public static void isDefault(ccaScenario scenario) {
        Boolean isCaseUpdateNeeded = scenario.caseId == null
                ? false
                : scenario.caseOwnerId == null
                        ? true
                        : !ccaJobContext.getInstance().getUsersStructure()
                                .checkUserBelongsInGroup(scenario.type, 'ccaDMCDefault', scenario.caseOwnerId);
        scenario.setAnalysisFields(
                'Is DMC Default' + (
                        isCaseUpdateNeeded
                                ? ' - Update case'
                                : (scenario.caseId == null)
                                ? ' - Create Case'
                                : ccaJobContext.getInstance().getUsersStructure().checkUserBelongsInGroup(scenario.type, 'ccaDMCDefault', scenario.caseOwnerId)
                                        ? ' - Already assigned to ' + scenario.caseOwnerName + ' in group: ccaDMCDefault' : '' //Unreachable
                )
                , isCaseUpdateNeeded
                , scenario.caseId == null
                , null
                , scenario.caseId == null ? null : isCaseUpdateNeeded ? scenario.caseOwnerId : null
                , isCaseUpdateNeeded || scenario.caseId == null ? 'ccaDMCDefault' : null
        );
    }
}