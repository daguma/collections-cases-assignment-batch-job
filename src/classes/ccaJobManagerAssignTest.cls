@isTest
public class ccaJobManagerAssignTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_start() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(logList.isEmpty());

        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(Contract__c = contract.Id, ScenarioAssignmentDescription__c = 'Not Empty');
        insert log;
        loan__loan_account__c contract2 = LibraryTest.createContractTH();
        ccaLog__c log2 = new ccaLog__c(Contract__c = contract2.Id, ScenarioAssignmentDescription__c = '');
        insert log2;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        Set<Id> contractsInLogsSelected = new Set<Id>();
        for (ccaLog__c logItem : (List<ccaLog__c>) logList) {
            contractsInLogsSelected.add(logItem.contract__c);
        }
        system.assert(!contractsInLogsSelected.contains(contract.Id));
        system.assert(contractsInLogsSelected.contains(contract2.Id));
        Test.stopTest();
    }

    static testMethod void validate_get_case() {
        Test.startTest();
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, assignedTo__c = userId, isCaseCreationNeeded__c = true);
        insert log;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        Case testCase = ccaJobManagerAssign.getCase((ccaLog__c) logList[0]);
        insert testCase;
        system.assertEquals(userId, testCase.ownerId);
        system.assertEquals(Label.RecordType_Collections_Id, testCase.recordTypeId);
        system.assertEquals('A collection case has been created for ' + contract.name, testCase.subject);
        system.assertEquals('A collection case has been created for ' + contract.name, testCase.description);
        system.assertEquals('New', testCase.status);
        system.assertEquals('Collections', testCase.origin);
        system.assertEquals('Medium', testCase.priority);
        system.assertEquals(contract.Opportunity__c, testCase.Opportunity__c);
        system.assertEquals(contract.loan__Contact__c, testCase.contactId);
        system.assertEquals(true, testCase.Automatic_Assignment__c);
        system.assertEquals(Date.today(), testCase.Case_Assignment_Date__c);

        log = [SELECT Id FROM ccaLog__c][0];
        log.isCaseCreationNeeded__c = false;
        log.case__c = testCase.Id;
        update log;
        QL = job.start(bc);
        QIT = QL.iterator();
        logList.clear();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        Case testCase2 = ccaJobManagerAssign.getCase((ccaLog__c) logList[0]);
        system.assertEquals(testCase.Id, testCase2.Id);
        system.assertEquals(userId, testCase2.ownerId);
        system.assertEquals('New', testCase2.status);
        system.assertEquals(Date.today(), testCase2.Case_Assignment_Date__c);
        system.assertEquals(true, testCase2.Automatic_Assignment__c);
        system.assertEquals('Collections', testCase2.origin);
        system.assertEquals('Medium', testCase2.priority);
        system.assertEquals(userId, testCase2.PreviousOwner__c);
        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable_2() {
        Test.startTest();
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                ASSIGN_scope__c = 100
        );
        insert cuSettings;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_finish() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        system.assertEquals(ccaUtil.JOB_ACTION_ASSIGN, job.jobAction);
        ccaJobManagerAssign.finish(null, job);
        system.assertEquals(ccaUtil.JOB_ACTION_REPORT, job.jobAction);
        Test.stopTest();
    }

    static testMethod void validate_finish_2() {
        Test.startTest();
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                REPORT_scope__c = 1500
        );
        insert cuSettings;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        system.assertEquals(ccaUtil.JOB_ACTION_ASSIGN, job.jobAction);
        ccaJobManagerAssign.finish(null, job);
        system.assertEquals(ccaUtil.JOB_ACTION_REPORT, job.jobAction);
        Test.stopTest();
    }

    static testMethod void validate_execute_update_and_create_true() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, isCaseCreationNeeded__c = true, isCaseUpdateNeeded__c = true);
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);
        ccaLog__c resultLog = [SELECT Id, ScenarioAssignmentDescription__c FROM ccaLog__c][0];
        system.assertEquals('Not Reassigned - Can not identify if update or insert is needed', resultLog.ScenarioAssignmentDescription__c);
        Test.stopTest();
    }

    static testMethod void validate_execute_update_and_create_false() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, isCaseCreationNeeded__c = false, isCaseUpdateNeeded__c = false);
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);

        ccaLog__c resultLog = [SELECT Id, ScenarioAssignmentDescription__c FROM ccaLog__c][0];
        system.assertEquals('Not Reassigned', resultLog.ScenarioAssignmentDescription__c);

        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        loan__loan_account__c contract2 = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        contract2.Collection_Case_Owner__c = userId;
        update contract2;
        ccaLog__c log2 = new ccaLog__c(contract__c = contract2.Id, isCaseCreationNeeded__c = false, isCaseUpdateNeeded__c = false);
        insert log2;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);

        ccaLog__c resultLog2 = [SELECT Id, ScenarioAssignmentDescription__c FROM ccaLog__c WHERE contract__c = :contract2.Id][0];
        system.assertEquals('Not Reassigned - Collection Case Owner in contract is updated', resultLog2.ScenarioAssignmentDescription__c);
        loan__loan_account__c resultContract2 = [SELECT Id, Collection_Case_Owner__c FROM loan__loan_account__c WHERE Id = :contract2.Id];
        system.assertEquals(null, resultContract2.Collection_Case_Owner__c);
        Test.stopTest();
    }

    static testMethod void validate_execute_create() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, isCaseCreationNeeded__c = true, isCaseUpdateNeeded__c = false, loadBalanceGroup__c = 'ccaDTC1');
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);

        ccaLog__c resultLog = [SELECT Id, ScenarioAssignmentDescription__c, CaseCreated__c, assignedTo__c FROM ccaLog__c][0];
        system.assertEquals('Assigned', resultLog.ScenarioAssignmentDescription__c);
        system.assertNotEquals(null, resultLog.assignedTo__c);
        system.assertNotEquals(null, [SELECT Id, Collection_Case_Owner__c FROM loan__loan_account__c WHERE Id = :contract.Id]);
        system.assertEquals(resultLog.CaseCreated__c, [SELECT Id FROM Case].Id);
    }

    static testMethod void validate_execute_create_with_caseOwnerNew() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, isCaseCreationNeeded__c = true, isCaseUpdateNeeded__c = false, loadBalanceGroup__c = 'ccaDTC1', caseOwnerNew__c = userId);
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);

        ccaLog__c resultLog = [SELECT Id, ScenarioAssignmentDescription__c, CaseCreated__c, assignedTo__c FROM ccaLog__c][0];
        system.assertEquals('Assigned', resultLog.ScenarioAssignmentDescription__c);
        system.assertEquals(userId, resultLog.assignedTo__c);
        system.assertNotEquals(null, [SELECT Id, Collection_Case_Owner__c FROM loan__loan_account__c WHERE Id = :contract.Id]);
        system.assertEquals(resultLog.CaseCreated__c, [SELECT Id FROM Case].Id);
        Test.stopTest();
    }

    static testMethod void validate_execute_update() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, isCaseCreationNeeded__c = false, isCaseUpdateNeeded__c = true, loadBalanceGroup__c = 'ccaDTC1');
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);

        ccaLog__c resultLog = [SELECT Id, ScenarioAssignmentDescription__c, CaseCreated__c, assignedTo__c FROM ccaLog__c][0];
        system.assertEquals('Assigned', resultLog.ScenarioAssignmentDescription__c);
        system.assertNotEquals(null, resultLog.assignedTo__c);
        system.assertNotEquals(null, [SELECT Id, Collection_Case_Owner__c FROM loan__loan_account__c WHERE Id = :contract.Id]);
        system.assertEquals(null, resultLog.CaseCreated__c);
    }

    static testMethod void validate_execute_update_with_caseOwnerNew() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ASSIGN);
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        loan__loan_account__c contract = [SELECT Id, name, opportunity__c, loan__contact__c from loan__loan_account__c where id = :LibraryTest.createContractTH().Id];
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id, isCaseCreationNeeded__c = false, isCaseUpdateNeeded__c = true, loadBalanceGroup__c = 'ccaDTC1', caseOwnerNew__c = userId);
        insert log;
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        job.execute(BC, logList);

        ccaLog__c resultLog = [SELECT Id, ScenarioAssignmentDescription__c, CaseCreated__c, assignedTo__c FROM ccaLog__c][0];
        system.assertEquals('Assigned', resultLog.ScenarioAssignmentDescription__c);
        system.assertEquals(userId, resultLog.assignedTo__c);
        system.assertNotEquals(null, [SELECT Id, Collection_Case_Owner__c FROM loan__loan_account__c WHERE Id = :contract.Id]);
        system.assertEquals(null, resultLog.CaseCreated__c);
        Test.stopTest();
    }

}