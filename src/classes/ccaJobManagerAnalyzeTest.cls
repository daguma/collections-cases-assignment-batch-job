@isTest
public class ccaJobManagerAnalyzeTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_start() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        Database.BatchableContext BC;
        List<loan__loan_account__c> logList = new List<loan__loan_account__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((loan__loan_account__c) QIT.next());
        }
        system.assert(logList.isEmpty());

        loan__loan_account__c contract = LibraryTest.createContractTH();
        contract.dmc__c = 'It is DMC';
        contract.loan__loan_status__c = 'Closed - Obligations met';
        update contract;
        Id userIdNotHal = [SELECT Id FROM User WHERE name != 'HAL' LIMIT 1].Id;
        loan__loan_account__c nondmc_contract = LibraryTest.createContractTH();
        nondmc_contract.dmc__c = '';
        nondmc_contract.loan__loan_status__c = 'Active - Bad Standing';
        nondmc_contract.Collection_Case_Owner__c = userIdNotHal;
        update nondmc_contract;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((loan__loan_account__c) QIT.next());
        }
        Set<Id> contractsSelected = new Set<Id>();
        for (loan__loan_account__c contractItem : (List<loan__loan_account__c>) logList) {
            contractsSelected.add(contractItem.Id);
        }

        system.assert(!contractsSelected.contains(contract.Id));
        system.assert(contractsSelected.contains(nondmc_contract.Id));

        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable_2() {
        Test.startTest();
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                ANALYZE_scope__c = 100
        );
        insert cuSettings;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_finish() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        system.assertEquals(ccaUtil.JOB_ACTION_ANALYZE, job.jobAction);
        ccaJobManagerAnalyze.finish(null, job);
        system.assertEquals(ccaUtil.JOB_ACTION_ASSIGN, job.jobAction);
        Test.stopTest();
    }

    static testMethod void validate_finish_2() {
        Test.startTest();
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                ASSIGN_scope__c = 100
        );
        insert cuSettings;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        system.assertEquals(ccaUtil.JOB_ACTION_ANALYZE, job.jobAction);
        ccaJobManagerAnalyze.finish(null, job);
        system.assertEquals(ccaUtil.JOB_ACTION_ASSIGN, job.jobAction);
        Test.stopTest();
    }

    static testMethod void validate_execute() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_ANALYZE);
        loan__loan_account__c contract = LibraryTest.createContractTH();
        contract.Collection_Case_Owner__c = [SELECT Id FROM User WHERE name != 'HAL' LIMIT 1].Id;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-2);
        contract.loan__Disbursal_Status__c = 'Completed';
        contract.loan__Disbursal_Amount__c = 5000;
        contract.loan__loan_status__c = 'Active - Bad Standing';
        update contract;
        Case aCase = new Case(CL_Contract__c = contract.Id, RecordTypeId = Label.RecordType_Collections_Id);
        insert aCase;
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name = 'ACH'];
        loan__Loan_Payment_Transaction__c aPayment = new loan__Loan_Payment_Transaction__c();
        aPayment.loan__loan_account__c = contract.Id;
        aPayment.loan__balance__c = 100;
        aPayment.loan__Transaction_Amount__c = 100;
        aPayment.loan__Principal__c = 100;
        aPayment.loan__Interest__c = 10;
        aPayment.loan__Reversed__c = false;
        aPayment.loan__Cleared__c = true;
        aPayment.loan__Payment_Mode__c = pm.Id;
        aPayment.loan__Transaction_Date__c = Date.today().addDays(-5);
        insert aPayment;
        system.debug(JSON.serialize([SELECT Id, Overdue_Days__c, DMC__c, Collection_Case_Owner__c,Product_Name__c, name, loan__loan_status__c FROM loan__loan_account__c]));
        Database.BatchableContext BC;
        List<loan__loan_account__c> logList = new List<loan__loan_account__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((loan__loan_account__c) QIT.next());
        }
        system.debug(JSON.serialize(logList));
        job.execute(null, logList);
        List<ccaLog__c> logList2 = [SELECT Id, type__c, lastPaymentReversalReason__c, ScenarioAnalysisDescription__c, isCaseCreationNeeded__c, isCaseUpdateNeeded__c, LoadBalanceGroup__c from ccaLog__c];
        system.debug(JSON.serialize(logList2));
        system.assert(!logList.isEmpty());
        Test.stopTest();
    }

}