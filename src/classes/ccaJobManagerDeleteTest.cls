@isTest
public class ccaJobManagerDeleteTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_start() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(logList.isEmpty());

        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(Contract__c = contract.Id);
        insert log;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(!logList.isEmpty());
        Test.stopTest();
    }

    static testMethod void validate_execute() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        Database.BatchableContext BC;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        Database.QueryLocator QL = job.start(bc);
        Database.QueryLocatorIterator QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(logList.isEmpty());

        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(Contract__c = contract.Id);
        insert log;
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(!logList.isEmpty());
        job.execute(BC, logList);

        logList.clear();
        QL = job.start(bc);
        QIT = QL.iterator();
        while (QIT.hasNext()) {
            logList.add((ccaLog__c) QIT.next());
        }
        system.assert(logList.isEmpty());
        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_execute_schedulable_2() {
        Test.startTest();
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                DELETE_scope__c = 1500
        );
        insert cuSettings;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        String sch = '0 0 10 ? 1/6 MON#1 *';
        system.schedule('Test ccaJob', sch, job);
        Test.stopTest();
    }

    static testMethod void validate_finish() {
        Test.startTest();
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        system.assertEquals(ccaUtil.JOB_ACTION_DELETE, job.jobAction);
        ccaJobManagerDelete.finish(null, job);
        system.assertEquals(ccaUtil.JOB_ACTION_ANALYZE, job.jobAction);
        Test.stopTest();
    }
    static testMethod void validate_finish_2() {
        Test.startTest();
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true,
                ANALYZE_scope__c = 350
        );
        insert cuSettings;
        ccaJob job = new ccaJob(ccaUtil.JOB_ACTION_DELETE);
        system.assertEquals(ccaUtil.JOB_ACTION_DELETE, job.jobAction);
        ccaJobManagerDelete.finish(null, job);
        system.assertEquals(ccaUtil.JOB_ACTION_ANALYZE, job.jobAction);
        Test.stopTest();
    }

}