@isTest
public class ccaScenarioTest {

    @isTest static void validate_constructor() {
        Test.startTest();
        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaScenario scenario = new ccaScenario(contract, null, null);
        system.assertEquals(contract.Id, scenario.contractId);
        system.assertEquals(contract.loan__loan_status__c, scenario.contractStatus);
        system.assertEquals(contract.Collection_Case_Owner__c, scenario.collectionsCaseOwnerId);
        system.assertEquals(Integer.valueOf(contract.overdue_days__c), scenario.dpd);
        system.assertEquals(ccaUtil.SCENARIO_TYPE_DTC, scenario.type);
        system.assert(String.isEmpty(scenario.caseId));
        system.assert(String.isEmpty(scenario.caseOwnerId));
        system.assert(String.isEmpty(scenario.caseOwnerName));
        system.assert(String.isEmpty(scenario.previousCaseOwner));
        system.assert(String.isEmpty(scenario.previousCaseOwnerName));
        system.assert(String.isEmpty(scenario.lastPaymentReversalReason));
        system.assert(!scenario.isLastPaymentReturned);
        Test.stopTest();
    }

    @isTest static void validate_constructor_2() {
        Test.startTest();
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        Case aCase = new Case(PreviousOwner__c = userId);
        insert aCase;
        loan__loan_account__c contract2 = LibraryTest.createContractTH();
        contract2.DMC__c = 'It is DMC';
        ccaScenario scenario2 = new ccaScenario(contract2, aCase, null);
        system.assertEquals(contract2.Id, scenario2.contractId);
        system.assertEquals(contract2.loan__loan_status__c, scenario2.contractStatus);
        system.assertEquals(contract2.Collection_Case_Owner__c, scenario2.collectionsCaseOwnerId);
        system.assertEquals(Integer.valueOf(contract2.overdue_days__c), scenario2.dpd);
        system.assertEquals(ccaUtil.SCENARIO_TYPE_DMC, scenario2.type);
        system.assertEquals(aCase.Id, scenario2.caseId);
        system.assertEquals(aCase.Owner.Id, scenario2.caseOwnerId);
        system.assertEquals(aCase.Owner.name, scenario2.caseOwnerName);
        system.assertEquals(aCase.previousowner__c, scenario2.previousCaseOwner);
        system.assertEquals(aCase.previousowner__r.name, scenario2.previousCaseOwnerName);
        system.assert(String.isEmpty(scenario2.lastPaymentReversalReason));
        system.assert(!scenario2.isLastPaymentReturned);
        Test.stopTest();
    }

    @isTest static void validate_constructor_3() {
        Test.startTest();
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        Case aCase = new Case(PreviousOwner__c = userId);
        insert aCase;
        loan__Loan_Payment_Transaction__c aPayment = new loan__Loan_Payment_Transaction__c(loan__Reversal_Reason__c = 'A valid reversal reason R01');
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        Lending_Product__c lp = new Lending_Product__c();
        lp.Name = 'Point Of Need';
        insert lp;
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        update opp;
        loan__loan_Account__c contract3 = LibraryTest.createContractTH();
        contract3.loan__Loan_Status__c = 'Active Good Standing';
        contract3.Lending_Product__c = lp.Id;
        contract3.loan__Contact__c = con.Id;
        contract3.Opportunity__c = opp.Id;
        upsert contract3;
        contract3 = [
                SELECT Id, Overdue_Days__c, loan__loan_status__c, Collection_Case_Owner__c, DMC__c, Product_Name__c
                FROM loan__loan_account__c
                WHERE Id = :contract3.Id
                LIMIT 1
        ];
        ccaScenario scenario3 = new ccaScenario(contract3, aCase, aPayment);
        system.assertEquals(contract3.Id, scenario3.contractId);
        system.assertEquals(contract3.loan__loan_status__c, scenario3.contractStatus);
        system.assertEquals(contract3.Collection_Case_Owner__c, scenario3.collectionsCaseOwnerId);
        system.assertEquals(Integer.valueOf(contract3.overdue_days__c), scenario3.dpd);
        system.assertEquals(ccaUtil.SCENARIO_TYPE_PON, scenario3.type);
        system.assertEquals(aCase.Id, scenario3.caseId);
        system.assertEquals(aCase.Owner.Id, scenario3.caseOwnerId);
        system.assertEquals(aCase.Owner.name, scenario3.caseOwnerName);
        system.assertEquals(aCase.previousowner__c, scenario3.previousCaseOwner);
        system.assertEquals(aCase.previousowner__r.name, scenario3.previousCaseOwnerName);
        system.assert(!String.isEmpty(scenario3.lastPaymentReversalReason));
        system.assert(scenario3.isLastPaymentReturned);
        Test.stopTest();
    }

    @isTest static void validate_constructor_4() {
        Test.startTest();
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        Case aCase = new Case(PreviousOwner__c = userId);
        insert aCase;
        loan__loan_account__c contract = LibraryTest.createContractTH();
        loan__Loan_Payment_Transaction__c aPayment2 = new loan__Loan_Payment_Transaction__c(loan__Reversal_Reason__c = 'A non valid reversal reason R88');
        ccaScenario scenario4 = new ccaScenario(contract, aCase, aPayment2);
        system.assertEquals(contract.Id, scenario4.contractId);
        system.assertEquals(contract.loan__loan_status__c, scenario4.contractStatus);
        system.assertEquals(contract.Collection_Case_Owner__c, scenario4.collectionsCaseOwnerId);
        system.assertEquals(Integer.valueOf(contract.overdue_days__c), scenario4.dpd);
        system.assert((ccaUtil.SCENARIO_TYPE_DMC + ',' + ccaUtil.SCENARIO_TYPE_DTC + ',' + ccaUtil.SCENARIO_TYPE_PON).contains(scenario4.type));
        system.assertEquals(aCase.Id, scenario4.caseId);
        system.assertEquals(aCase.Owner.Id, scenario4.caseOwnerId);
        system.assertEquals(aCase.Owner.name, scenario4.caseOwnerName);
        system.assertEquals(aCase.previousowner__c, scenario4.previousCaseOwner);
        system.assertEquals(aCase.previousowner__r.name, scenario4.previousCaseOwnerName);
        system.assert(!String.isEmpty(scenario4.lastPaymentReversalReason));
        system.assert(!scenario4.isLastPaymentReturned);
        Test.stopTest();
    }

    @isTest static void validate_set_analysis_fields() {
        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaScenario scenario = new ccaScenario(contract, null, null);
        system.assertEquals(null, scenario.analysisDescription);
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);
        scenario.setAnalysisFields('Some analysis', true);
        system.assertEquals('Some analysis', scenario.analysisDescription);
        system.assertEquals(true, scenario.updateCase);
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);

        ccaScenario scenario2 = new ccaScenario(contract, null, null);
        system.assertEquals(null, scenario2.analysisDescription);
        system.assertEquals(false, scenario2.updateCase);
        system.assertEquals(false, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(null, scenario2.previousCaseOwner);
        system.assertEquals(null, scenario2.loadBalanceGroup);
        scenario2.setAnalysisFields('Some analysis', true, true);
        system.assertEquals('Some analysis', scenario2.analysisDescription);
        system.assertEquals(true, scenario2.updateCase);
        system.assertEquals(true, scenario2.createCase);
        system.assertEquals(null, scenario2.caseOwnerNewId);
        system.assertEquals(null, scenario2.previousCaseOwner);
        system.assertEquals(null, scenario2.loadBalanceGroup);

        ccaScenario scenario3 = new ccaScenario(contract, null, null);
        system.assertEquals(null, scenario3.analysisDescription);
        system.assertEquals(false, scenario3.updateCase);
        system.assertEquals(false, scenario3.createCase);
        system.assertEquals(null, scenario3.caseOwnerNewId);
        system.assertEquals(null, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        scenario3.setAnalysisFields('Some analysis', true, true, userId);
        system.assertEquals('Some analysis', scenario3.analysisDescription);
        system.assertEquals(true, scenario3.updateCase);
        system.assertEquals(true, scenario3.createCase);
        system.assertEquals(userId, scenario3.caseOwnerNewId);
        system.assertEquals(null, scenario3.previousCaseOwner);
        system.assertEquals(null, scenario3.loadBalanceGroup);

        ccaScenario scenario4 = new ccaScenario(contract, null, null);
        system.assertEquals(null, scenario4.analysisDescription);
        system.assertEquals(false, scenario4.updateCase);
        system.assertEquals(false, scenario4.createCase);
        system.assertEquals(null, scenario4.caseOwnerNewId);
        system.assertEquals(null, scenario4.previousCaseOwner);
        system.assertEquals(null, scenario4.loadBalanceGroup);
        Id userId2 = [SELECT Id FROM User WHERE Id != :userId LIMIT 1].Id;
        scenario4.setAnalysisFields('Some analysis', true, true, userId, userId2);
        system.assertEquals('Some analysis', scenario4.analysisDescription);
        system.assertEquals(true, scenario4.updateCase);
        system.assertEquals(true, scenario4.createCase);
        system.assertEquals(userId, scenario4.caseOwnerNewId);
        system.assertEquals(userId2, scenario4.previousCaseOwner);
        system.assertEquals(null, scenario4.loadBalanceGroup);

        ccaScenario scenario5 = new ccaScenario(contract, null, null);
        system.assertEquals(null, scenario5.analysisDescription);
        system.assertEquals(false, scenario5.updateCase);
        system.assertEquals(false, scenario5.createCase);
        system.assertEquals(null, scenario5.caseOwnerNewId);
        system.assertEquals(null, scenario5.previousCaseOwner);
        system.assertEquals(null, scenario5.loadBalanceGroup);
        scenario5.setAnalysisFields('Some analysis', true, true, userId, userId2, 'some group');
        system.assertEquals('Some analysis', scenario5.analysisDescription);
        system.assertEquals(true, scenario5.updateCase);
        system.assertEquals(true, scenario5.createCase);
        system.assertEquals(userId, scenario5.caseOwnerNewId);
        system.assertEquals(userId2, scenario5.previousCaseOwner);
        system.assertEquals('some group', scenario5.loadBalanceGroup);

    }

    @isTest static void validate_to_CCALog() {
        loan__loan_account__c contract = LibraryTest.createContractTH();
        contract = [SELECT Id, Overdue_Days__c, DMC__c, Product_Name__c, loan__loan_status__c, Collection_Case_Owner__c FROM loan__loan_account__c LIMIT 1];
        Case aCase = new Case();
        insert aCase;
        Case caseCreated = new Case();
        insert caseCreated;
        Id userId_1 = [SELECT Id FROM User LIMIT 1].Id;
        Id userId_2 = [SELECT Id FROM User WHERE name != :userId_1 LIMIT 1].Id;
        Id userId_3 = [SELECT Id FROM User WHERE name != :userId_1 AND name != :userId_2 LIMIT 1].Id;
        Id userId_4 = [SELECT Id FROM User WHERE name != :userId_1 AND name != :userId_2 AND name != :userId_3 LIMIT 1].Id;
        ccaScenario scenario = new ccaScenario(contract, aCase, null);
        scenario.caseOwnerId = userId_1;
        scenario.collectionsCaseOwnerId = userId_2;
        scenario.analysisDescription = 'Analysis description';
        scenario.assignmentDescription = 'Assignment description';
        scenario.lastPaymentReversalReason = 'Last payment reversal reason R02';
        scenario.caseOwnerNewId = userId_3;
        scenario.createCase = true;
        scenario.updateCase = true;
        scenario.loadBalanceGroup = 'Load balance group';
        scenario.assignedTo = userId_4;
        scenario.caseCreated = caseCreated.Id;
        ccaLog__c log = scenario.toCCALog(1);
        system.assertNotEquals(null, log);
        system.assertEquals(contract.Id, log.contract__c);
        system.assertEquals(contract.Overdue_days__c, log.dpd__c);
        system.assertEquals(userId_1, log.caseOwner__c);
        system.assertEquals(userId_2, log.collectionsCaseOwner__c);
        system.assertEquals(aCase.Id, log.Case__c);
        system.assertEquals('Analysis description', log.ScenarioAnalysisDescription__c);
        system.assertEquals('Assignment description', log.ScenarioAssignmentDescription__c);
        system.assertEquals('Last payment reversal reason R02', log.lastPaymentReversalReason__c);
        system.assertEquals(userId_3, log.caseOwnerNew__c);
        system.assertEquals(true, log.isCaseCreationNeeded__c);
        system.assertEquals(true, log.isCaseUpdateNeeded__c);
        system.assertEquals('Load balance group', log.LoadBalanceGroup__c);
        system.assertEquals('ccaDTC', log.type__c);
        system.assertEquals(userId_4, log.AssignedTo__c);
        system.assertEquals(caseCreated.Id, log.CaseCreated__c);
        system.assertEquals('1', log.BatchNumberAnalyze__c);
    }

}