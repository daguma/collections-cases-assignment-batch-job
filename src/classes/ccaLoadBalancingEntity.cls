/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaLoadBalancingEntity.cls
 *
 * This entity holds the result of the ccaLoadBalancingAlgorithm.process().
 *
 *  - loadBalancedMap: the KEY section of this Map has the User Ids and the VALUES section indicates how many Cases
 * can be assigned to them following the loan balancing rules in the ccaLoadBalancingAlgorithm.cls
 *
 *  - leastLoadedUserId: The load balancing algorithm is not exact, so when the LoadBalancedMap is empty and has no
 *  more users to assign cases to, it will return this leastLoadedUserId instead.
 *
 * </p>
 */
public class ccaLoadBalancingEntity {

    @TestVisible Map<Id, Long> loadBalancedMap;
    Id leastLoadedUserId;

    public ccaLoadBalancingEntity(Map<Id, Long> loadBalance, Id leastLoadedUser) {
        this.leastLoadedUserId = leastLoadedUser;
        this.loadBalancedMap = loadBalance;
    }

    //It returns a Collector user Id that is available to receive more Cases.
    //Each time it returns the Id, the VALUES section of its corresponding KEY is subtracted by 1, until it reached 0.
    //At that point, if no one in the Map is available, it returns the leastLoadedUserId.
    public Id getNextAssigneeId() {
        Id nextAvailableCollectorId;
        if (this.loadBalancedMap == null || this.loadBalancedMap.isEmpty()) {
            return nextAvailableCollectorId;
        }
        List<Id> idsList = new List<Id>(this.loadBalancedMap.keySet());
        Integer listItem = 0;
        Long load = 0;
        do {
            nextAvailableCollectorId = idsList.get(listItem);
            load = (Long) this.loadBalancedMap.get(nextAvailableCollectorId);
            listItem++;
        } while (listItem < idsList.size() && load == 0);
        if (load == 0) {
            return this.leastLoadedUserId;
        } else {
            this.loadBalancedMap.remove(nextAvailableCollectorId);
            this.loadBalancedMap.put(nextAvailableCollectorId, load.intValue() - 1);
            return nextAvailableCollectorId;
        }
    }
}