@isTest
public class ccaJobSummaryReportTest {

    @isTest static void validate_get_instance() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobSummaryReport.getInstance());
        Test.stopTest();
    }

    @isTest static void validate_add_report_line() {
        Test.startTest();
        system.assert(ccaJobSummaryReport.getInstance().logReport.isEmpty());
        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id);
        ccaJobSummaryReport.getInstance().addReportLine(log);
        system.assertEquals(2, ccaJobSummaryReport.getInstance().logReport.size());
        Test.stopTest();
    }

    @isTest static void validate_log_report_to_string() {
        Test.startTest();
        system.assert(ccaJobSummaryReport.getInstance().logReport.isEmpty());
        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaLog__c log = new ccaLog__c(contract__c = contract.Id);
        ccaJobSummaryReport.getInstance().addReportLine(log);
        system.assertEquals(2, ccaJobSummaryReport.getInstance().logReport.size());
        system.assert(String.isNotEmpty(ccaJobSummaryReport.getInstance().logReportToString()));
        Test.stopTest();
    }

    @isTest static void validate_to_string() {
        Test.startTest();
        ccaJobSummaryReport.getInstance().analyzedDMC = 5;
        system.assert(String.isNotEmpty(ccaJobSummaryReport.getInstance().toString()));
        system.assert(ccaJobSummaryReport.getInstance().toString().contains('DMC: 5'));
        Test.stopTest();
    }

    @isTest static void validate_count_analyzed_scenario() {
        Test.startTest();
        system.assertEquals(0, ccaJobSummaryReport.getInstance().analyzedDMC);
        system.assertEquals(0, ccaJobSummaryReport.getInstance().analyzedDTC);
        system.assertEquals(0, ccaJobSummaryReport.getInstance().analyzedPON);
        system.assertEquals(0, ccaJobSummaryReport.getInstance().notAnalyzedDMC);
        system.assertEquals(0, ccaJobSummaryReport.getInstance().notAnalyzedDTC);
        system.assertEquals(0, ccaJobSummaryReport.getInstance().notAnalyzedPON);
        system.assertEquals(0, ccaJobSummaryReport.getInstance().unrecognizedNotAnalyzed);

        loan__loan_account__c contract = LibraryTest.createContractTH();
        ccaScenario scenario = new ccaScenario(contract, null, null);
        scenario.analysisDescription = 'analyzed';
        scenario.type = ccaUtil.SCENARIO_TYPE_DMC;
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().analyzedDMC);
        scenario.type = ccaUtil.SCENARIO_TYPE_DTC;
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().analyzedDTC);
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().analyzedPON);
        scenario.analysisDescription = '';
        scenario.type = ccaUtil.SCENARIO_TYPE_DMC;
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().notAnalyzedDMC);
        scenario.type = ccaUtil.SCENARIO_TYPE_DTC;
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().notAnalyzedDTC);
        scenario.type = ccaUtil.SCENARIO_TYPE_PON;
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().notAnalyzedPON);
        scenario.type = 'Something invalid';
        ccaJobSummaryReport.getInstance().countAnalyzedScenario(scenario);
        system.assertEquals(1, ccaJobSummaryReport.getInstance().unrecognizedNotAnalyzed);

        Test.stopTest();
    }

}