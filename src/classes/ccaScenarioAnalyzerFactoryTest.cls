@isTest
public class ccaScenarioAnalyzerFactoryTest {

    @isTest static void validate_get_scenario_analyzer() {
        Test.startTest();
        system.assertNotEquals(null, ccaScenarioAnalyzerFactory.getScenarioAnalyzer(ccaUtil.SCENARIO_TYPE_PON));
        system.assert(ccaScenarioAnalyzerFactory.getScenarioAnalyzer(ccaUtil.SCENARIO_TYPE_DMC) instanceof ccaAnalyzeDMC);
        system.assert(ccaScenarioAnalyzerFactory.getScenarioAnalyzer(ccaUtil.SCENARIO_TYPE_DTC) instanceof ccaAnalyzeDTC);
        system.assert(ccaScenarioAnalyzerFactory.getScenarioAnalyzer(ccaUtil.SCENARIO_TYPE_PON) instanceof ccaAnalyzePON);

        try {
            ccaScenarioAnalyzerFactory.getScenarioAnalyzer(null);
        } catch (Exception e) {
            system.assertEquals('ccaScenarioAnalyzerFactory - Scenario type has to be provided', e.getMessage());
        }

        try {
            ccaScenarioAnalyzerFactory.getScenarioAnalyzer('');
        } catch (Exception e) {
            system.assertEquals('ccaScenarioAnalyzerFactory - Scenario type has to be provided', e.getMessage());
        }

        try {
            ccaScenarioAnalyzerFactory.getScenarioAnalyzer('Some other scenario type');
        } catch (Exception e) {
            system.assertEquals('ccaScenarioAnalyzerFactory - Provided scenario type unrecognized', e.getMessage());
        }

        Test.stopTest();
    }

}