public class ccaJobSummaryReport {

    private static ccaJobSummaryReport instance = null;

    @TestVisible private Integer analyzedDMC, analyzedPON, analyzedDTC, notAnalyzedDMC, notAnalyzedDTC, notAnalyzedPON, unrecognizedNotAnalyzed;
    @TestVisible private List<String> logReport;

    private ccaJobSummaryReport() {
        analyzedDMC = 0; analyzedPON = 0; analyzedDTC = 0;
        notAnalyzedDMC = 0; notAnalyzedDTC = 0; notAnalyzedPON = 0;
        unrecognizedNotAnalyzed = 0;
        logReport = new List<String>();
    }

    public static ccaJobSummaryReport getInstance() {
        if (instance == null) instance = new ccaJobSummaryReport();
        return instance;
    }

    public void addReportLine(ccaLog__c log) {
        if (this.logReport.isEmpty()) {
            this.logReport.add(
                    'Contract,Contract Status,DPD,DMC,Product Name,Type,Last Payment Reversal Reason,Case,Case Category,Case Owner' +
                            ',Previous Owner,Collections Case Owner,Case Owner New,Load Balance Group,Analysis,Assignment' +
                            ' ,Update Case,Create Case,Assigned To,Case Created,Analyze Batch Number,Assign Batch Number\n'
            );
        }
        this.logReport.add(
                '"' + log.Contract__r.name + '","' + log.ContractStatus__c + '","' + log.dpd__c + '","' +
                        log.Contract__r.DMC__c + '","' + log.Contract__r.Product_Name__c + '","' +
                        log.type__c + '","' + log.lastPaymentReversalReason__c + '","' + log.Case__r.CaseNumber + '","' +
                        log.Case__r.ccaLoadBalanceGroup__c + '","' + log.CaseOwner__r.name + '","' +
                        log.Case__r.PreviousOwner__r.name + '","' + log.CollectionsCaseOwner__r.name + '","' +
                        log.CaseOwnerNew__r.name + '","' + log.LoadBalanceGroup__c + '","' +
                        log.ScenarioAnalysisDescription__c + '","' + log.ScenarioAssignmentDescription__c + '","' +
                        log.isCaseUpdateNeeded__c + '","' + log.isCaseCreationNeeded__c + '","' +
                        log.AssignedTo__r.name + '","' + log.CaseCreated__r.CaseNumber + '","' +
                        log.BatchNumberAnalyze__c + '","' + log.BatchNumberAssign__c + '"\n'
        );
    }

    public String logReportToString() {
        String logReport = '';
        if (this.logReport.size() > 0) {
            logReport = this.logReport.remove(0);//header
        }
        for (Integer i = this.logReport.size(); i > 0; i--) {
            logReport += this.logReport.remove(i - 1);
        }
        return logReport;
    }

    public void countAnalyzedScenario(ccaScenario scenario) {
        if (String.isNotEmpty(scenario.analysisDescription)) {
            //scenario successfully recognized and analyzed
            if (ccaUtil.SCENARIO_TYPE_DMC.equalsIgnoreCase(scenario.type))
                analyzedDMC++; else if (ccaUtil.SCENARIO_TYPE_DTC.equalsIgnoreCase(scenario.type))
                analyzedDTC++; else if (ccaUtil.SCENARIO_TYPE_PON.equalsIgnoreCase(scenario.type))
                analyzedPON++;
        } else {
            //scenario not analyzed
            if (ccaUtil.SCENARIO_TYPE_DMC.equalsIgnoreCase(scenario.type))
                notAnalyzedDMC++; else if (ccaUtil.SCENARIO_TYPE_DTC.equalsIgnoreCase(scenario.type))
                notAnalyzedDTC++; else if (ccaUtil.SCENARIO_TYPE_PON.equalsIgnoreCase(scenario.type))
                notAnalyzedPON++; else
                    unrecognizedNotAnalyzed++;
        }

    }

    public override String toString() {
        return 'Collections Cases Assignment Job'
                + ' | DMC: ' + analyzedDMC + '|' + notAnalyzedDMC
                + ' | PON: ' + analyzedPON + '|' + notAnalyzedPON
                + ' | DTC: ' + analyzedDTC + '|' + notAnalyzedDTC
                + ' | Unrecognized: ' + unrecognizedNotAnalyzed;
    }

}