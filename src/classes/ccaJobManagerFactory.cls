/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaJobManagerFactory.cls
 *
 * Creates the manager for the job.
 * The job manager will handle start, execute and finish methods for the job.
 *
 * A class of type "ccaIJobManager" is returned, and its name is taken from the ccaUtil.JOB_MANAGERS_MAP
 *
 * </p>
 */
public class ccaJobManagerFactory {

    public static ccaIJobManager getJobManager(String jobAction) {
        if (string.isNotEmpty(jobAction)) {
            if (String.isNotEmpty(ccaUtil.JOB_MANAGERS_MAP.get(jobAction))) {
                return (ccaIJobManager) Type.forName(ccaUtil.JOB_MANAGERS_MAP.get(jobAction)).newInstance();
            } else {
                throw new JobManagerFactoryException('ccaJobManagerFactory - Provided job action unrecognized');
            }
        }
        throw new JobManagerFactoryException('ccaJobManagerFactory - Job action has to be provided');
    }

    public class JobManagerFactoryException extends Exception {
    }

}