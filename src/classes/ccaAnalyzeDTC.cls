public class ccaAnalyzeDTC implements ccaIScenarioAnalyzer {

    public static ccaScenario analyze(ccaScenario scenario) {
        if (scenario.contractStatus != null && !scenario.contractStatus.containsIgnoreCase('active')) {
            isDefault(scenario);
        } else if (scenario.dpd == 0) {
            isCured(scenario);
        } else if (scenario.isLastPaymentReturned) {
            isReturnedPayment(scenario);
        } else {
            isDpdTeam(scenario);
        }
        return scenario;
    }

    public static void isDefault(ccaScenario scenario) {
        Boolean isCaseUpdateNeeded = scenario.caseId == null
                ? false
                : scenario.caseOwnerId == null
                        ? true
                        : !ccaJobContext.getInstance().getUsersStructure()
                                .checkUserBelongsInGroup(scenario.type, 'ccaDTCDefault', scenario.caseOwnerId);
        scenario.setAnalysisFields(
                'Is DTC Default' + (
                        isCaseUpdateNeeded
                                ? ' - Update case'
                                : (scenario.caseId == null)
                                ? ' - Create Case'
                                : ccaJobContext.getInstance().getUsersStructure().checkUserBelongsInGroup(scenario.type, 'ccaDTCDefault', scenario.caseOwnerId)
                                        ? ' - Already assigned to ' + scenario.caseOwnerName + ' in group: ccaDTCDefault' : ''
                )
                , isCaseUpdateNeeded
                , scenario.caseId == null
                , null
                , scenario.caseId == null ? null : isCaseUpdateNeeded ? scenario.caseOwnerId : null
                , isCaseUpdateNeeded || scenario.caseId == null ? 'ccaDTCDefault' : null
        );
    }

    public static void isCured(ccaScenario scenario) {
        //Cured - Assign this case to HAL.
        //If it is already assigned to any other person, make sure to save that Case.Owner in the Case.PreviousOwner__c field
        scenario.setAnalysisFields(
                'DTC - Cured - Assign to HAL'
                , scenario.caseId == null
                        ? false
                        : !Label.HAL_Id.equalsIgnoreCase(
                                String.valueOf(scenario.caseOwnerId).substring(0, String.valueOf(scenario.caseOwnerId).length() - 3))
                , scenario.caseId == null
                , Label.HAL_Id
                , scenario.caseId == null ? null : scenario.caseOwnerId
        );
    }

    public static void isReturnedPayment(ccaScenario scenario) {
        if (ccaJobContext.getInstance().getUsersStructure()
                .checkUserBelongsInDpdLevel(scenario.type, scenario.dpd, scenario.caseOwnerId)) {
            scenario.setAnalysisFields(
                    'DTC - Return payments - Already correctly assigned to ' + scenario.caseOwnerName
                            + ' in ' + ccaJobContext.getInstance().getUsersStructure().getGroupNameByDpd(scenario.type, scenario.dpd)
                    , false //Do not reassign
                    , false //Do not create case
                    , null
                    , null
            );
        } else {
            if (ccaJobContext.getInstance().getUsersStructure()
                    .checkUserBelongsInDpdLevel(scenario.type, scenario.dpd, scenario.previousCaseOwner)) {
                scenario.setAnalysisFields(
                        'DTC - Return payments - Assign to Previous Owner'
                        , !scenario.caseOwnerId.equals(scenario.previousCaseOwner)
                        , false //If user matched dpd, by definition the case already exists
                        , scenario.previousCaseOwner //
                        , scenario.caseOwnerId
                );
            } else {
                scenario.setAnalysisFields(
                        'DTC - Return payments - Assign to corresponding DPD Team'
                        , !(scenario.caseId == null)
                        , scenario.caseId == null
                        , null //At this point, we don't know who will the Loan Balancing Algorithm assign this case to
                        , scenario.previousCaseOwner
                        , ccaJobContext.getInstance().getUsersStructure().getGroupNameByDpd(scenario.type, scenario.dpd)
                );
            }
        }
    }

    public static void isDpdTeam(ccaScenario scenario) {
        String dpdCorrespondingTeam = ccaJobContext.getInstance().getUsersStructure().getGroupNameByDpd(scenario.type, scenario.dpd);
        if (scenario.caseId == null) {
            //Case does not exist. Create and assign to corresponding Team by Dpd
            scenario.setAnalysisFields(
                    'DTC - Dpd - Case does not exist - Create case - Load balance in ' + dpdCorrespondingTeam
                    , false, true, null, null, dpdCorrespondingTeam
            );
        } else {
            if (ccaJobContext.getInstance().getUsersStructure()
                    .checkUserBelongsInDpdLevel(scenario.type, scenario.dpd, scenario.caseOwnerId)) {
                //It is currently assigned to a user that belongs to the correct dpd team
                scenario.setAnalysisFields(
                        'DTC - Dpd - It is correctly assigned to ' + scenario.caseOwnerName + ' from ' + dpdCorrespondingTeam
                        , false, false, null, null, null
                );
            } else {
                //It is currently assigned to a user that DOES NOT belong to the correct dpd team
                scenario.setAnalysisFields(
                        'DTC - Dpd - Reassign - load balance in ' + dpdCorrespondingTeam
                        , true, false, null, scenario.caseOwnerId, dpdCorrespondingTeam
                );
            }
        }
    }

}