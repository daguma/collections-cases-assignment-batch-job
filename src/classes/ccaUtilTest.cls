@isTest
public class ccaUtilTest {

    @isTest static void validate_scenario_analyzers_map() {
        Test.startTest();
        system.assertEquals('ccaAnalyzeDTC', ccaUtil.SCENARIO_ANALYZERS_MAP.get(ccaUtil.SCENARIO_TYPE_DTC));
        system.assertEquals('ccaAnalyzeDMC', ccaUtil.SCENARIO_ANALYZERS_MAP.get(ccaUtil.SCENARIO_TYPE_DMC));
        system.assertEquals('ccaAnalyzePON', ccaUtil.SCENARIO_ANALYZERS_MAP.get(ccaUtil.SCENARIO_TYPE_PON));
        Test.stopTest();
    }

    @isTest static void validate_job_managers_map() {
        Test.startTest();
        system.assertEquals('ccaJobManagerDelete', ccaUtil.JOB_MANAGERS_MAP.get(ccaUtil.JOB_ACTION_DELETE));
        system.assertEquals('ccaJobManagerAnalyze', ccaUtil.JOB_MANAGERS_MAP.get(ccaUtil.JOB_ACTION_ANALYZE));
        system.assertEquals('ccaJobManagerAssign', ccaUtil.JOB_MANAGERS_MAP.get(ccaUtil.JOB_ACTION_ASSIGN));
        system.assertEquals('ccaJobManagerReport', ccaUtil.JOB_MANAGERS_MAP.get(ccaUtil.JOB_ACTION_REPORT));
        Test.stopTest();
    }

}