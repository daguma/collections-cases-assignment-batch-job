/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaScenarioAnalyzerFactory.cls
 *
 * Creates the analyzer for the scenario given.
 * An analyzer represents the Business Logic needed for a specific scenario type (DTC, PON, DMC)
 *
 * A class of type "ccaIScenarioAnalyzer" is returned, and its name is taken from the ccaUtil.SCENARIO_ANALYZERS_MAP
 *
 * </p>
 */
public class ccaScenarioAnalyzerFactory {

    public static ccaIScenarioAnalyzer getScenarioAnalyzer(String scenarioType) {
        if (string.isNotEmpty(scenarioType)) {
            if (String.isNotEmpty(ccaUtil.SCENARIO_ANALYZERS_MAP.get(scenarioType))) {
                return (ccaIScenarioAnalyzer) Type.forName(ccaUtil.SCENARIO_ANALYZERS_MAP.get(scenarioType)).newInstance();
            } else {
                throw new ScenarioAnalyzerFactoryException('ccaScenarioAnalyzerFactory - Provided scenario type unrecognized');
            }
        }
        throw new ScenarioAnalyzerFactoryException('ccaScenarioAnalyzerFactory - Scenario type has to be provided');
    }

    public class ScenarioAnalyzerFactoryException extends Exception {
    }

}