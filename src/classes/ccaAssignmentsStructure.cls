public class ccaAssignmentsStructure {

    public Map<String, ccaLoadBalancingEntity> loadBalancedMap;
    public Map<String, Map<Id, Integer>> assignedPerGroupBeforeProcessMap;
    public Map<String, Integer> totalInflowCasesToAssignPerGroupMap;
    public final String initialAssignmentsStructure;//At class level because the Final email displays its contents

    public ccaAssignmentsStructure(List<ccaUsersStructure.Team> teamsList) {
        AssignmentsStructureEntity entity = getAssignmentsStructure(teamsList);
        this.loadBalancedMap = entity.loadBalancedMap;
        this.assignedPerGroupBeforeProcessMap = entity.assignedPerGroupBeforeProcessMap;
        this.totalInflowCasesToAssignPerGroupMap = entity.totalInflowCasesToAssignPerGroupMap;
        this.initialAssignmentsStructure = JSON.serialize(this);
    }

    public static AssignmentsStructureEntity getAssignmentsStructure(List<ccaUsersStructure.Team> teamsList) {
        Map<String, ccaLoadBalancingEntity> loadBalancedMap = new Map<String, ccaLoadBalancingEntity>();
        Map<String, Map<Id, Integer>> assignedPerGroupBeforeProcessMap = new Map<String, Map<Id, Integer>>();
        Map<String, Integer> totalInflowCasesToAssignPerGroupMap = new Map<String, Integer>();
        Map<Id, Integer> currentAssignmentMap;
        Map<string, AggregateResult> currentAssignmentAggregateResultMap = new Map<string, AggregateResult>([SELECT OwnerId Id, count(Id) myCount FROM Case WHERE RecordTypeId = :Label.RecordType_Collections_Id AND CL_Contract__c != NULL AND isClosed = false GROUP BY OwnerId]);
        Map<string, AggregateResult> inflowLoadAggregateResultMap = new Map<string, AggregateResult>([SELECT loadBalanceGroup__c Id, count(id) myCount FROM ccaLog__c WHERE loadBalanceGroup__c != '' AND loadBalanceGroup__c != NULL GROUP BY loadBalanceGroup__c]);
        for (ccaUsersStructure.Team team : teamsList) {
            for (ccaUsersStructure.TeamGroup usersGroup : team.getAllGroupsList()) {
                currentAssignmentMap = new Map<Id, Integer>();
                for (Id userId : usersGroup.getAllGroupMembersIds()) {
                    currentAssignmentMap.put(
                            userId
                            , currentAssignmentAggregateResultMap.get(userId) != null
                                    ? (Integer) currentAssignmentAggregateResultMap.get(userId).get('myCount')
                                    : 0
                    );
                }
                assignedPerGroupBeforeProcessMap.put(usersGroup.getGroupName(), currentAssignmentMap);
                totalInflowCasesToAssignPerGroupMap.put(
                        usersGroup.getGroupName()
                        , inflowLoadAggregateResultMap.get(usersGroup.getGroupName()) != null
                                ? (Integer) inflowLoadAggregateResultMap.get(usersGroup.getGroupName()).get('myCount')
                                : 0
                );
                loadBalancedMap.put(
                        usersGroup.getGroupName()
                        , ccaLoadBalancingAlgorithm.process(
                                inflowLoadAggregateResultMap.get(usersGroup.getGroupName()) != null
                                        ? (Integer) inflowLoadAggregateResultMap.get(usersGroup.getGroupName()).get('myCount')
                                        : 0
                                , currentAssignmentMap
                        )
                );
            }
        }
        return new AssignmentsStructureEntity(loadBalancedMap, assignedPerGroupBeforeProcessMap, totalInflowCasesToAssignPerGroupMap);
    }

    public String getInitialAssignmentsStructure() {
        return this.initialAssignmentsStructure;
    }

    public Id getNextAssigneeId(String groupName) {
        if (String.isEmpty(groupName)) return null;
        return this.loadBalancedMap.get(groupName) != null ? this.loadBalancedMap.get(groupName).getNextAssigneeId() : null;
    }

    public class AssignmentsStructureEntity {
        public Map<String, ccaLoadBalancingEntity> loadBalancedMap;
        public Map<String, Map<Id, Integer>> assignedPerGroupBeforeProcessMap;
        public Map<String, Integer> totalInflowCasesToAssignPerGroupMap;

        public AssignmentsStructureEntity(Map<String, ccaLoadBalancingEntity> loadBalancedMap
                , Map<String, Map<Id, Integer>> assignedPerGroupBeforeProcessMap
                , Map<String, Integer> totalInflowCasesToAssignPerGroupMap) {
            this.loadBalancedMap = loadBalancedMap;
            this.assignedPerGroupBeforeProcessMap = assignedPerGroupBeforeProcessMap;
            this.totalInflowCasesToAssignPerGroupMap = totalInflowCasesToAssignPerGroupMap;
        }
    }
}