@isTest
public class ccaAnalyzeDMCTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    @isTest static void validate_is_dmc_team() {
        Test.startTest();

        loan__loan_account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.DMC__c = 'IT IS DMC';
        update contract;
        ccaScenario scenario = new ccaScenario(contract, null, null);
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        system.assertEquals(null, scenario.analysisDescription);

        scenario.contractStatus = 'Closed- Written Off';
        ccaAnalyzeDMC.isDMCTeam(scenario);
        system.assertEquals('Is DMC Team - Create case', scenario.analysisDescription);

        scenario.contractStatus = 'Active - Bad Standing';
        ccaAnalyzeDMC.isDMCTeam(scenario);
        system.assertEquals('Is DMC Team - Create case', scenario.analysisDescription);

        //Active - Good Standing, no case existing --> It should create (not update) the case, no previousOwner, ccaDMC1 must be the loadBalanceGroup, caseOwnerNewId must be null
        ccaAnalyzeDMC.isDMCTeam(scenario);
        system.assertEquals('Is DMC Team - Create case', scenario.analysisDescription);
        system.assertEquals(false, scenario.updateCase);
        system.assertEquals(true, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaDMC1', scenario.loadBalanceGroup);

        //Active - Good Standing, case assigned to a collector in the correct group
        // --> It should not create and it should not update the case, no previousOwner, '' must be the loadBalanceGroup, caseOwnerNewId must be null
        Group ccaDMC1 = [SELECT Id FROM Group WHERE developername = 'DMC_0_180'];
        Map<Id, AggregateResult> collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDMC1.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        Id collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        Case aCase = new Case();
        insert aCase;
        scenario = new ccaScenario(contract, aCase, null);
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        scenario.caseOwnerId = collectorUserId; //Field is not writeable: Case.Owner
        scenario.caseOwnerName = 'Test_User'; //Field is not writeable: Case.Owner
        ccaAnalyzeDMC.isDMCTeam(scenario);
        system.assertEquals('Is DMC Team - Already assigned to ' + scenario.caseOwnerName + ' in group: ccaDMC1', scenario.analysisDescription);
        system.assertEquals(false, scenario.updateCase); //The scenario.caseOwnerId belongs to the DMC1 team
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals('', scenario.loadBalanceGroup);

        //Active - Good Standing, case assigned to someone not in the correct group --> It should update (not create) the case, previousOwner is the caseOwnerId before the analysis, 'ccaDMC1' must be the loadBalanceGroup, caseOwnerNewId must be null
        User notCollector = [SELECT Id FROM User WHERE Name = 'HAL'];
        scenario.caseOwnerId = notCollector.Id; //Field is not writeable: Case.Owner
        ccaAnalyzeDMC.isDMCTeam(scenario);
        system.assertEquals('Is DMC Team - Update case', scenario.analysisDescription);
        system.assertEquals(true, scenario.updateCase); //The scenario.caseOwnerId does not belong to the DMC1 team
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(notCollector.Id, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals('ccaDMC1', scenario.loadBalanceGroup);

        scenario.caseOwnerId = null; //Field is not writeable: Case.Owner
        ccaAnalyzeDMC.isDMCTeam(scenario);
        system.assertEquals('Is DMC Team - Update case', scenario.analysisDescription);
        system.assertEquals(true, scenario.updateCase);
        system.assertEquals(false, scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaDMC1', scenario.loadBalanceGroup);

        Test.stopTest();
    }

    @isTest static void validate_is_default() {
        Test.startTest();

        loan__loan_account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_status__c = 'Closed - Settled Off';
        contract.DMC__c = 'IT IS DMC';
        update contract;
        ccaScenario scenario = new ccaScenario(contract, null, null);
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        system.assertEquals(null, scenario.analysisDescription);
        system.assertEquals(null, scenario.loadBalanceGroup);

        scenario.contractStatus = 'Closed - Settled Off';
        ccaAnalyzeDMC.isDefault(scenario);
        system.assertEquals('Is DMC Default - Create Case', scenario.analysisDescription);
        system.assertEquals('ccaDMCDefault', scenario.loadBalanceGroup);

        scenario.contractStatus = 'Closed - Sold Off';
        ccaAnalyzeDMC.isDefault(scenario);
        system.assertEquals('Is DMC Default - Create Case', scenario.analysisDescription);
        system.assertEquals('ccaDMCDefault', scenario.loadBalanceGroup);

        Case aCase = new Case();
        insert aCase;
        scenario = new ccaScenario(contract, aCase, null);
        scenario.contractStatus = 'Closed - Sold Off';
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        ccaAnalyzeDMC.isDefault(scenario);
        system.assertEquals('Is DMC Default - Update case', scenario.analysisDescription);
        system.assert(scenario.updateCase);
        system.assert(!scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals('ccaDMCDefault', scenario.loadBalanceGroup);

        Group ccaDMC1 = [SELECT Id FROM Group WHERE developername = 'DMC_Default'];
        Map<Id, AggregateResult> collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDMC1.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        Id collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        scenario.caseOwnerId = collectorUserId; //Field is not writeable: Case.Owner
        scenario.caseOwnerName = 'TestCollector'; //Field is not writeable: Case.Owner
        ccaAnalyzeDMC.isDefault(scenario);
        system.assertEquals('Is DMC Default - Already assigned to ' + scenario.caseOwnerName + ' in group: ccaDMCDefault', scenario.analysisDescription);
        system.assert(!scenario.updateCase);
        system.assert(!scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(null, scenario.previousCaseOwner);
        system.assertEquals(null, scenario.loadBalanceGroup);

        ccaDMC1 = [SELECT Id FROM Group WHERE developername = 'DMC_0_180'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDMC1.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        scenario.caseOwnerId = collectorUserId; //Field is not writeable: Case.Owner
        scenario.caseOwnerName = 'TestCollector'; //Field is not writeable: Case.Owner
        ccaAnalyzeDMC.isDefault(scenario);
        system.assertEquals('Is DMC Default - Update case', scenario.analysisDescription);
        system.assert(scenario.updateCase);
        system.assert(!scenario.createCase);
        system.assertEquals(null, scenario.caseOwnerNewId);
        system.assertEquals(collectorUserId, scenario.caseOwnerId);
        system.assertEquals('ccaDMCDefault', scenario.loadBalanceGroup);

        Test.stopTest();
    }

    @isTest static void validate_analyze() {
        Test.startTest();
        loan__loan_account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_status__c = 'Closed - Settled Off';
        contract.DMC__c = 'IT IS DMC';
        update contract;
        ccaScenario scenario = new ccaScenario(contract, null, null);
        scenario.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        ccaAnalyzeDMC.analyze(scenario);
        system.assertNotEquals(null, scenario.analysisDescription);

        loan__loan_account__c contract2 = LibraryTest.createContractTH();
        contract2.loan__loan_status__c = 'Active - Bad Standing';
        contract2.DMC__c = 'IT IS DMC';
        update contract2;
        ccaScenario scenario2 = new ccaScenario(contract2, null, null);
        scenario2.dpd = 0; //Field is not writeable: loan__Loan_Account__c.Overdue_Days__c
        ccaAnalyzeDMC.analyze(scenario2);
        system.assertNotEquals(null, scenario2.analysisDescription);
        Test.stopTest();
    }
}