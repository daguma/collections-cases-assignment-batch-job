@isTest
public class ccaJobContextTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    @isTest static void validate_constructor() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobContext.getInstance());
        Test.stopTest();
    }

    @isTest static void validate_share_instance() {
        Test.startTest();
        ccaJobContext test1 = ccaJobContext.getInstance();
        ccaJobContext.shareInstance(test1);
        system.assertEquals(test1, ccaJobContext.getInstance());
        Test.stopTest();
    }

    @isTest static void validate_get_Job_Summary_Report() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobContext.getInstance().getJobSummaryReport());
        Test.stopTest();
    }

    @isTest static void validate_get_Users_Structure() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobContext.getInstance().getUsersStructure());
        Test.stopTest();
    }

    @isTest static void validate_get_Logger() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobContext.getInstance().getLogger());
        Test.stopTest();
    }

    @isTest static void validate_get_custom_setting() {
        Test.startTest();
        system.assertNotEquals(null, ccaJobContext.getInstance().getCustomSetting());
        Test.stopTest();
    }

    @isTest static void validate_set_Up_Assignments_Structure() {
        Test.startTest();
        ccaJobContext.getInstance().setUpAssignmentsStructure();
        system.assertNotEquals(null, ccaJobContext.getInstance().getAssignmentsStructure());
        Test.stopTest();
    }

}