public class ccaJobContext {

    private static ccaJobContext instance = null;

    public ccaJobSummaryReport scenarioSummaryReport;
    public ccaUsersStructure usersStructure;
    public ccaAssignmentsStructure assignmentsStructure;
    public ccaLogger logger;
    public ccaCustomSetting__c ccaCustomSetting;

    private ccaJobContext() {
        ccaCustomSetting = ccaCustomSetting__c.getInstance('cca');
        scenarioSummaryReport = ccaJobSummaryReport.getInstance();
        usersStructure = ccaUsersStructure.getInstance(ccaCustomSetting.Main_public_group__c);
        logger = ccaLogger.getInstance();
    }

    public static ccaJobContext getInstance() {
        if (instance == null) instance = new ccaJobContext();
        return instance;
    }

    public static void shareInstance(ccaJobContext aInstance) {
        instance = aInstance;
    }

    public ccaJobSummaryReport getJobSummaryReport() {
        return this.scenarioSummaryReport;
    }

    public ccaUsersStructure getUsersStructure() {
        return this.usersStructure;
    }

    public ccaAssignmentsStructure getAssignmentsStructure() {
        return this.assignmentsStructure != null ? this.assignmentsStructure : new ccaAssignmentsStructure(this.getUsersStructure().getTeams().values());
    }

    public ccaLogger getLogger() {
        return this.logger;
    }

    public ccaCustomSetting__c getCustomSetting() {
        return this.ccaCustomSetting;
    }

    public void setUpAssignmentsStructure() {
        assignmentsStructure = new ccaAssignmentsStructure(getUsersStructure().getTeams().values());
    }

}