public class ccaJobManagerAssign implements ccaIJobManager {

    public static Database.QueryLocator start(Database.BatchableContext BC, ccaJob job) {

        //Use the same working context and set up the ccaAssignmentsStructure to be used by the batches
        ccaJobContext.shareInstance(job.jobContext);
        ccaJobContext.getInstance().setUpAssignmentsStructure();

        //Build the query, log it and then return the QueryLocator
        String query = 'SELECT Id, isCaseUpdateNeeded__c, isCaseCreationNeeded__c, CaseCreated__c, ScenarioAssignmentDescription__c, AssignedTo__c'
                + ' , caseOwnerNew__c, Case__c, Case__r.OwnerId, contract__c, contract__r.name , contract__r.Opportunity__c, contract__r.loan__Contact__c'
                + ' , contract__r.Collection_Case_Owner__c, loadBalanceGroup__c FROM ccaLog__c'
                + ' WHERE ScenarioAssignmentDescription__c = NULL OR ScenarioAssignmentDescription__c = \'\'';
        ccaJobContext.getInstance().getLogger().logEvent('ccaJobContext.getInstance().getLogger() - ccaJobManagerAssign - start - Query: ' + query);
        return Database.getQueryLocator(query);
    }

    public static void execute(Database.BatchableContext BC, List<Object> scope, ccaJob job) {

        //Use the already loaded working context in every batch
        ccaJobContext.shareInstance(job.jobContext);

        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - Batch number: ' + job.batchNumber + ' - execute (START) - Limits.getCpuTime(): ' + Limits.getCpuTime());

        //Assignment process
        List<Case> upsertTheseCases = new List<Case>();
        List<loan__loan_account__c> updateTheseContracts = new List<loan__loan_account__c>();
        for (ccaLog__c log : (List<ccaLog__c>) scope) {

            //If both isCaseUpdateNeeded__c && isCaseCreationNeeded__c are true, handle that mistake here
            if (log.isCaseUpdateNeeded__c && log.isCaseCreationNeeded__c) {
                log.ScenarioAssignmentDescription__c = 'Not Reassigned - Can not identify if update or insert is needed';

            } else if (log.isCaseUpdateNeeded__c || log.isCaseCreationNeeded__c) {//Update or Create is needed

                //If caseOwnerNew__c is empty then get the Collector Id that will be assigned to this Case from the ccaAssignmentsStructure
                log.assignedTo__c = log.caseOwnerNew__c != null
                        ? log.caseOwnerNew__c
                        : ccaJobContext.getInstance().getAssignmentsStructure().getNextAssigneeId(log.loadBalanceGroup__c) ;
                log.ScenarioAssignmentDescription__c = 'Assigned';

                //Add the Case to the working list
                upsertTheseCases.add(getCase(log));

                //If contract.Collection_Case_Owner__c is not the same as the Case.Owner add the Contract to the working list
                if (log.contract__r.Collection_Case_Owner__c != log.assignedTo__c) {
                    updateTheseContracts.add(new loan__Loan_Account__c(id = log.contract__c, Collection_Case_Owner__c = log.assignedTo__c));
                }

            } else {//If no Update and no Create is needed, just check for the contract.Collection_Case_Owner__c
                log.ScenarioAssignmentDescription__c = 'Not Reassigned';

                //If contract.Collection_Case_Owner__c is not the same as the Case.Owner add the Contract to the working list
                if (log.contract__r.Collection_Case_Owner__c != log.Case__r.OwnerId) {
                    log.ScenarioAssignmentDescription__c = 'Not Reassigned - Collection Case Owner in contract is updated';
                    updateTheseContracts.add(new loan__Loan_Account__c(id = log.contract__c, Collection_Case_Owner__c = log.Case__r.OwnerId));
                }
            }
        }

        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (ASSIGN logic finished) - Limits.getCpuTime(): ' + Limits.getCpuTime());

        //Start the Database process.
        //At this point, the Cases, Contracts and ccaLog__c lists are ready to be sent to the Database
        Map<Id, Id> newCasesIdsForScope = new Map<Id, Id>();
        Boolean continueProcess = true;

        //First upsert the Case records and add them to newCasesIdsForScope in order to update the ccaLog__c after, if the custom setting allows
        try {
            if (ccaJobContext.getInstance().getCustomSetting().Update_Cases__c) {

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (START upsertTheseCases) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                Database.UpsertResult[] upsertTheseCasesResult = Database.upsert(upsertTheseCases, false);

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (END upsertTheseCases) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (START upsertTheseCases success check) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                for (Integer i = 0; i < upsertTheseCasesResult.size(); i++) {
                    if (!upsertTheseCasesResult[i].isSuccess()) {
                        ccaJobContext.getInstance().getLogger().logEvent('ccaJobManagerAssign - upsert upsertTheseCases - ' + JSON.serialize(upsertTheseCases[i]) + ' - ' + upsertTheseCasesResult[i].getErrors());
                    } else {
                        newCasesIdsForScope.put(upsertTheseCases[i].CL_Contract__c, upsertTheseCases[i].Id);
                    }
                }

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (END upsertTheseCases success check) - Limits.getCpuTime(): ' + Limits.getCpuTime());

            }
        } catch (Exception e) {
            ccaJobContext.getInstance().getLogger().logEvent('ASSIGN batch number: ' + job.batchNumber + ', Exception: ' + e.getMessage());
            continueProcess = false;
        }

        if (continueProcess) {
            try {

                //Update the BatchNumberAssign__c in the scope
                //Update the ccaLog__c records with the "upserted" Case records
                for (ccaLog__c myScopeItem : (List<ccaLog__c>) scope) {
                    myScopeItem.BatchNumberAssign__c = String.valueOf(job.batchNumber);
                    if (myScopeItem.isCaseCreationNeeded__c) {
                        myScopeItem.CaseCreated__c = newCasesIdsForScope.get(myScopeItem.contract__c);
                    }
                }

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (START scope) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                Database.SaveResult[] updateTheScopeResult = Database.update((List<ccaLog__c>) scope, false);

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (END scope) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (START scope success check) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                for (Integer i = 0; i < updateTheScopeResult.size(); i++) {
                    if (!updateTheScopeResult[i].isSuccess()) {
                        ccaJobContext.getInstance().getLogger().logEvent('ccaJobManagerAssign - update scope - ' + JSON.serialize(scope[i]) + ' - ' + updateTheScopeResult[i].getErrors());
                    }
                }

                ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (STOP scope success check) - Limits.getCpuTime(): ' + Limits.getCpuTime());

            } catch (Exception e) {
                ccaJobContext.getInstance().getLogger().logEvent('ASSIGN batch number: ' + job.batchNumber + ', Exception: ' + e.getMessage());
            }
        }

        if (continueProcess) {
            try {

                //Update the contracts to correct their Collection_Case_Owner__c, if custom setting allows
                if (ccaJobContext.getInstance().getCustomSetting().Update_Contracts__c) {

                    ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (START updateTheseContracts) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                    Database.SaveResult[] updateTheseContractsResult = Database.update(updateTheseContracts, false);

                    ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (END updateTheseContracts) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                    ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (START updateTheseContracts success check) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                    for (Integer i = 0; i < updateTheseContractsResult.size(); i++) {
                        if (!updateTheseContractsResult[i].isSuccess()) {
                            ccaJobContext.getInstance().getLogger().logEvent('ccaJobManagerAssign - update updateTheseContracts - ' + JSON.serialize(updateTheseContracts[i]) + ' - ' + updateTheseContractsResult[i].getErrors());
                        }
                    }

                    ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (STOP updateTheseContracts success check) - Limits.getCpuTime(): ' + Limits.getCpuTime());

                }
            } catch (Exception e) {
                ccaJobContext.getInstance().getLogger().logEvent('ASSIGN batch number: ' + job.batchNumber + ', Exception: ' + e.getMessage());
            }
        }

        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getDMLRows(): ' + Limits.getDMLRows());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getLimitDMLRows(): ' + Limits.getLimitDMLRows());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getDMLStatements(): ' + Limits.getDMLStatements());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getLimitDMLStatements(): ' + Limits.getLimitDMLStatements());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getCpuTime(): ' + Limits.getCpuTime());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getLimitCpuTime(): ' + Limits.getLimitCpuTime());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getQueries(): ' + Limits.getQueries());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getLimitQueries(): ' + Limits.getLimitQueries());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getQueryLocatorRows(): ' + Limits.getQueryLocatorRows());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getLimitQueryLocatorRows(): ' + Limits.getLimitQueryLocatorRows());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getQueryRows(): ' + Limits.getQueryRows());
        ccaJobContext.getInstance().getLogger().logDebugInfo('ccaJobManagerAssign - execute (end) - Limits.getLimitQueryRows(): ' + Limits.getLimitQueryRows());
    }

    @TestVisible private static Case getCase(ccaLog__c log) {
        if (log.isCaseCreationNeeded__c) {
            return new Case(
                    OwnerId = log.assignedTo__c,
                    RecordTypeId = Label.RecordType_Collections_Id,
                    CL_Contract__c = log.contract__c,
                    Subject = 'A collection case has been created for ' + log.contract__r.name,
                    Description = 'A collection case has been created for ' + log.contract__r.name,
                    Status = 'New',
                    Origin = 'Collections',
                    Priority = 'Medium',
                    Opportunity__c = log.contract__r.Opportunity__c,
                    ContactId = log.contract__r.loan__Contact__c,
                    Automatic_Assignment__c = true,
                    Case_Assignment_Date__c = Date.today(),
                    ccaLoadBalanceGroup__c = log.loadBalanceGroup__c
            );
        } else {
            return new Case(
                    Id = log.Case__c,
                    OwnerId = log.assignedTo__c,
                    Status = 'New',
                    Automatic_Assignment__c = true,
                    Case_Assignment_Date__c = Date.today(),
                    PreviousOwner__c = log.Case__r.OwnerId,
                    ccaLoadBalanceGroup__c = log.loadBalanceGroup__c,
                    Origin = 'Collections',
                    Priority = 'Medium'
            );
        }
    }

    public static void execute(SchedulableContext sc, ccaJob job) {
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().ASSIGN_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().ASSIGN_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().ASSIGN_scope__c.intValue()
                        : 100
        );
    }

    public static void finish(Database.BatchableContext BC, ccaJob job) {
        job.jobAction = ccaUtil.JOB_ACTION_REPORT;
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().REPORT_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().REPORT_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().REPORT_scope__c.intValue()
                        : 1000
        );
    }

}