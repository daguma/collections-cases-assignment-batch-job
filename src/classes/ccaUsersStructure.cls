/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaUsersStructure.cls
 *
 * Using the Public Groups Salesforce functionality, a hierarchy of groups is created and then replicated in this class.
 *
 * There should be a parent main group. The child Teams depend on the busines, for example DMC, DTC, PON...
 * Each Team will have groups of people, for example: DTC_1_15 has collectors that work cases from 1 to 15 dpd only
 *
 * This users structure is built at the beginning of the ANALYZE job and kept intact after that. It is common for all
 * the jobs after that, accessed: ccaJobContext.getInstance.getUsersStructure()
 *
 * Utility methods are provided in order to get group names or users. And also to check if a specific user belongs to
 * a certain group within a Team.
 *
 * A Map-like structure is loaded and used in order to access data directly and void the many executions of FOR LOOPS or
 * Select statements during the jobs execution. It has an impact on the memory, but we are aiming to have a quick process.
 * It also helps to make this dynamic and reactive to changes in the Public Groups from Salesforce Setup view.
 *
 * The Users Structure can be accessed this way: ccaUsersStructure.setUpTeams('CollectionsCasesAssignment')
 *  - Foe example: system.debug(JSON.serialize( ccaUsersStructure.setUpTeams('CollectionsCasesAssignment') ));
 * </p>
 */
public class ccaUsersStructure {

    private static ccaUsersStructure instance = null;

    @TestVisible Map<String, Team> teamsMap;

    private ccaUsersStructure(String mainTeamName) {
        this.teamsMap = setUpTeams(mainTeamName);
    }

    public static ccaUsersStructure getInstance(String mainTeamName) {
        if (instance == null) {
            instance = new ccaUsersStructure(mainTeamName);
        }
        return instance;
    }

    public Map<String, Team> getTeams() {
        return this.teamsMap;
    }

    public static Map<String, Team> setUpTeams(String mainGroup) {
        Map<String, Team> teamsMap = new Map<String, Team>();
        if (String.isNotEmpty(mainGroup)) {
            Id mainPublicGroupId;
            try {
                mainPublicGroupId = [SELECT Id FROM Group WHERE Name = :mainGroup LIMIT 1].Id;
            } catch (System.QueryException e) {
                throw new ccaUsersStructureException('Collections Cases Assignment - ccaUsersStructure - MAIN PUBLIC GROUP DOES NO EXIST');
            }

            // Get the Public Groups data with AggregateResult for easier handling.
            // Keep the Id of the records (groups, users...) directly in the KEY section of the Map
            // Keep the actual records in the VALUES section of the Map
            Map<Id, AggregateResult> teamsInfoMap = new Map<Id, AggregateResult>([SELECT UserOrGroupId Id FROM GroupMember WHERE GroupId = :mainPublicGroupId GROUP BY UserOrGroupId]);
            Map<Id, AggregateResult> groupsInfoMap = new Map<Id, AggregateResult>([SELECT UserOrGroupId Id, GroupId FROM GroupMember WHERE GroupId in :teamsInfoMap.keySet() GROUP BY UserOrGroupId, GroupId]);
            Map<Id, Group> groupsNamesMap = new Map<Id, Group>([SELECT Id, name, developerName FROM Group WHERE Id in :groupsInfoMap.keySet()]);
            List<GroupMember> groupMembersList = [SELECT UserOrGroupId, GroupId FROM GroupMember WHERE GroupId IN :groupsInfoMap.keySet()];

            //Build the Users Structure: Team --> Group --> Users
            Team team;
            TeamGroup teamGroup;
            Boolean addGroupToTeam = false;
            for (Group aGroup : [SELECT Id, Name FROM Group WHERE Id in :teamsInfoMap.keySet()]) {
                team = new Team(aGroup.Name, aGroup.Id);
                for (AggregateResult ar : groupsInfoMap.values()) {
                    for (Group abGroup : groupsNamesMap.values()) {
                        addGroupToTeam = false;
                        if (String.valueOf(aGroup.Id).equals(String.valueOf(ar.get('GroupId')))
                                && String.valueOf(ar.get('Id')).equals(String.valueOf(abGroup.Id))) {
                            teamGroup = new TeamGroup(abGroup.name, abGroup.Id, abGroup.DeveloperName);
                            for (GroupMember gm : groupMembersList) {
                                if (String.valueOf(abGroup.Id).equals(String.valueOf(gm.GroupId))) {
                                    teamGroup.usersList.add(String.valueOf(gm.UserOrGroupId));
                                    addGroupToTeam = true;
                                }
                            }
                        }
                        if (addGroupToTeam) {
                            team.putGroupsByNameMap(teamGroup.name, teamGroup);
                        }
                    }
                }
                team.setUpGroupsByDpdMap();
                teamsMap.put(team.name, team);
            }
        } else {
            throw new ccaUsersStructureException('Collections Cases Assignment - ccaUsersStructure - NO MAIN PUBLIC GROUP FOUND');
        }
        return teamsMap;
    }

    public Boolean checkUserBelongsInDpdLevel(String teamName, Integer dpd, Id userId) {
        return (String.isEmpty(teamName) || this.teamsMap.get(teamName) == null || dpd == null)
                ? false : this.teamsMap.get(teamName).checkUserBelongsInDpdLevel(dpd, userId);
    }

    public String getGroupNameByDpd(String teamName, Integer dpd) {
        return String.isEmpty(teamName) || this.teamsMap.get(teamName) == null || dpd == null
                ? '' : this.teamsMap.get(teamName).getGroupNameByDpd(dpd);
    }

    public Boolean checkUserBelongsInGroup(String teamName, String groupName, Id userId) {
        return String.isEmpty(teamName) || this.teamsMap.get(teamName) == null || String.isEmpty(groupName) || String.isEmpty(userId)
                ? false
                : this.teamsMap.get(teamName).getGroupByName(groupName) == null
                        ? false : this.teamsMap.get(teamName).getGroupByName(groupName).containsUser(userId);
    }

    public class ccaUsersStructureException extends Exception {
    }

    @TestVisible
    public class Team {

        public String name;
        public Id teamId;
        public Map<String, TeamGroup> groupsByNameMap;
        public Map<Integer, TeamGroup> groupsByDpdMap;

        public Team(String name, Id teamId) {
            this.name = name;
            this.teamId = teamId;
            this.groupsByNameMap = new Map<String, TeamGroup>();
        }

        public void putGroupsByNameMap(String groupName, TeamGroup teamGroup) {
            this.groupsByNameMap.put(groupName, teamGroup);
        }

        //This method assumes that Groups DO NOT share DPD levels.
        // For example: 30 DPD is either Group A or Group B, but not both
        // Sort groupsByNameMap.values(), or else the getGroupByDpd will not work correctly.
        // Sort it here because this method executes only once regardless of the number of jobs.
        // The method getGroupByDpd will be called thousands of times
        public void setUpGroupsByDpdMap() {
            this.groupsByDpdMap = new Map<Integer, TeamGroup>();
            List<TeamGroup> teamGroupsList = groupsByNameMap.values();
            teamGroupsList.sort();
            for (TeamGroup tg : teamGroupsList) {
                if (String.valueOf(tg.dpdMin).isNumeric() && String.valueOf(tg.dpdMax).isNumeric()) {
                    if (tg.dpdMax > 0) {
                        for (Integer i = tg.dpdMin; i <= tg.dpdMax; i++) {
                            this.groupsByDpdMap.put(i, tg);
                        }
                    }
                }
            }
        }

        public TeamGroup getGroupByName(String groupName) {
            return String.isEmpty(groupName) ? null : this.groupsByNameMap.get(groupName);
        }

        //From the groupsByDpdMap, this method returns the corresponding Group for a DPD passed in as parameter.
        //A contract might have higher DPD than 180, in which case this method returns the last Group in the Team.
        public TeamGroup getGroupByDpd(Integer dpd) {
            if (dpd == null) return null;
            if (this.groupsByDpdMap == null || this.groupsByDpdMap.isEmpty()) return null;
            if (dpd > new List<Integer>(this.groupsByDpdMap.keySet())[this.groupsByDpdMap.keySet().size() - 1]) {
                return this.groupsByDpdMap.get(new List<Integer>(this.groupsByDpdMap.keySet())[this.groupsByDpdMap.keySet().size() - 1]);
            }
            return this.groupsByDpdMap.get(dpd);
        }

        public Boolean checkUserBelongsInDpdLevel(Integer dpd, Id userId) {
            if (dpd == null || String.isEmpty(userId)) return false;
            TeamGroup tg = getGroupByDpd(dpd);
            if (tg == null) return false;
            Boolean userBelongs = tg.containsUser(userId);
            tg = null;
            return userBelongs;
        }

        public String getGroupNameByDpd(Integer dpd) {
            if (dpd == null) return '';
            TeamGroup tg = getGroupByDpd(dpd);
            if (tg == null) return '';
            String groupName = tg.name;
            tg = null;
            return groupName;
        }

        public List<TeamGroup> getAllGroupsList() {
            return this.groupsByNameMap.values();
        }

    }

    public class TeamGroup implements Comparable {

        public String name;
        public Id groupId;
        public Integer dpdMin;
        public Integer dpdMax;
        public List<Id> usersList;

        public TeamGroup(String name, Id groupId, String dpdLevel) {
            this.name = name;
            this.groupId = groupId;
            this.dpdMin = 0;
            this.dpdMax = 0;
            if (dpdLevel.split('_') != null && dpdLevel.split('_').size() > 1) {
                if ((dpdLevel.split('_')[1]).isNumeric() && (dpdLevel.split('_')[2]).isNumeric()) {
                    this.dpdMin = (dpdLevel.split('_')[1]).isNumeric() ? Integer.valueOf(dpdLevel.split('_')[1]) : 0;
                    this.dpdMax = (dpdLevel.split('_')[2]).isNumeric() ? Integer.valueOf(dpdLevel.split('_')[2]) : 0;
                }
            }
            usersList = new List<Id>();
        }

        //0 if this instance and objectToCompareTo are equal
        //> 0 if this instance is greater than objectToCompareTo
        //< 0 if this instance is less than objectToCompareTo
        public Integer compareTo(Object compareTo) {
            //A negative Integer if the String that called the method lexicographically precedes secondString
            //A positive Integer if the String that called the method lexicographically follows compsecondStringString
            //Zero if the Strings are equal
            return this.name.compareTo(((TeamGroup) compareTo).name);
        }

        public String getGroupName() {
            return this.name;
        }

        public Boolean containsUser(Id userId) {
            return String.isEmpty(userId) ? false : new Set<Id>(this.usersList).contains(userId);
        }

        public List<Id> getAllGroupMembersIds() {
            return this.usersList;
        }
    }

}