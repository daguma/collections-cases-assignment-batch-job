/**
 * @author      Daniel Gutiérrez <dgutierrez@fuzionsoft.com>
 *
 * JIRA ticket: OF-1904 Collections | Assignment logic update
 *
 * <p>
 *
 * ccaLoadBalancingAlgorithm.cls
 *
 * This process is able to load balance an input into multiple recipients.
 *
 * By definition, before it distributes the Cases, it needs to know how many Cases are there (@param totalInflow).
 * This is why we have an ANALYSIS job and then an ASSIGNMENT job.
 *
 * Once it knows how many Cases in total it will assign, then it needs to know how many cases are already assigned to
 * each one of the Collectors (@param currentIndividualsLoadMap).
 *
 * 1. Find the maximum amount of cases assigned among the collectors in the team. We do this by getting the max Integer
 * in the @param currentIndividualsLoadMap.
 *
 * 2. Multiply that max by 105%. Just for fun, let's call this "Max Factor" :)
 *
 * 3. Find the quotient for each Id in the @param currentIndividualsLoadMap.
 * So, take each collector's load and divide it by the Max Factor.
 *
 * 4. For each calculated quotient, do: ABS ( 1 - quotient )
 * ABS: get absolute value
 *
 * 5. Breathe...
 *
 * 6. Get the sum of all the absolute values in the group, calculated in the previous step.
 *
 * 7. The number of load balanced possible cases assignments for each collector in the group is then given by:
 * Its absolute value divided by the group's sum of absolute values, multiplied by the total number of
 * cases to be assigned in the group (@param totalInflow)
 *
 * 8. Rounded up! --> System.RoundingMode.HALF_UP
 *
 * 9. During testing, we found that this algorithm is not exact. So, if at the end of the process,
 * the total sum of individual number of load balanced possible cases assignments is greater than
 * the @param totalInflow , we need to fix it here.
 * If the process calculated a higher number of assignments, then whoever had the most will be decreased.
 * If the process calculated a lower number of assignments, then whoever had the least will de adjusted up.
 *
 * </p>
 */
public class ccaLoadBalancingAlgorithm {

    public static ccaLoadBalancingEntity process(Integer totalInflow, Map<Id, Integer> currentIndividualsLoadMap) {

        if (totalInflow == null || currentIndividualsLoadMap == null) return null;

        if (totalInflow <= 0 || currentIndividualsLoadMap.isEmpty()) return new ccaLoadBalancingEntity(new Map<Id, Long>(), null);

        Integer maxCurrentIndividualLoad = 0;
        Decimal maxCurrentIndividualLoadModified = 0.00, individualLoadByMaxModifiedAbsSum = 0.00;
        Map<Id, Decimal> individualLoadByMaxModifiedMap, individualLoadByMaxModifiedAbsMap;
        Map<Id, Long> individualFutureLoadMap;
        Id individualWithLowestFutureLoad, individualWithHighestFutureLoad;

        //Get the maximum amount of cases that are already assigned to a user in the group
        Integer individualLoad = 0;
        for (Id individualId : currentIndividualsLoadMap.keySet()) {
            individualLoad = currentIndividualsLoadMap.get(individualId) != null ? currentIndividualsLoadMap.get(individualId) : 0;
            if (maxCurrentIndividualLoad < individualLoad) {
                maxCurrentIndividualLoad = individualLoad;
            }
        }

        //Modify that maximum by a factor of 105%
        //If the max current individual load is zero, then maxCurrentIndividualLoadModified MUST be 1 because it will be a divisor
        maxCurrentIndividualLoadModified = maxCurrentIndividualLoad == 0 ? 1 : Double.valueOf(maxCurrentIndividualLoad) * 105 / 100;

        //Get the quotient(1), where the dividend is the current load and the divisor is the modified max
        //Then get the inverse of that quotient(1) and add up all this numbers as a "group total"
        individualLoadByMaxModifiedMap = new Map<Id, Decimal>();
        individualLoadByMaxModifiedAbsMap = new Map<Id, Decimal>();
        Decimal individualLoanByMaxModified;
        for (Id individualId : currentIndividualsLoadMap.keySet()) {
            individualLoanByMaxModified = maxCurrentIndividualLoadModified == 0.0 ? 0 : currentIndividualsLoadMap.get(individualId) / maxCurrentIndividualLoadModified;
            individualLoadByMaxModifiedMap.put(individualId, individualLoanByMaxModified);

            individualLoadByMaxModifiedAbsMap.put(individualId, math.abs(1 - individualLoanByMaxModified));

            individualLoadByMaxModifiedAbsSum += math.abs(1 - individualLoanByMaxModified);
        }

        //Calculate the quotient(2) where the dividend is the "inverse" and the divisor is the "group total"
        //The future load is calculated by: Original Load * quotient(2)
        individualFutureLoadMap = new Map<Id, Long>();
        Long lowestFutureLoad, highestFutureLoad; Boolean firstIteration = true;
        Long totalAssignedLoad = 0;
        for (Id individualId : currentIndividualsLoadMap.keySet()) {

            Decimal temp = individualLoadByMaxModifiedAbsMap.get(individualId) / individualLoadByMaxModifiedAbsSum;
            temp = temp * totalInflow;
            individualFutureLoadMap.put(individualId, temp.round(System.RoundingMode.HALF_UP));
            totalAssignedLoad = totalAssignedLoad + temp.round(System.RoundingMode.HALF_UP);
            if (firstIteration) {
                firstIteration = false;
                lowestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                highestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                individualWithLowestFutureLoad = individualId;
                individualWithHighestFutureLoad = individualId;
            } else {
                if (temp.round(System.RoundingMode.HALF_UP) < lowestFutureLoad) {
                    lowestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                    individualWithLowestFutureLoad = individualId;
                }
                if (temp.round(System.RoundingMode.HALF_UP) > highestFutureLoad) {
                    highestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                    individualWithHighestFutureLoad = individualId;
                }
            }
        }

        //If the algorithm assigned more or less items than the inflow, then fix it here
        Long loadDifference = (long) totalInflow - totalAssignedLoad;
        if (loadDifference < 0) {
            //This means that the algorithm assigned more load than the totalInflow
            Long assignedLoad = individualFutureLoadMap.get(individualWithHighestFutureLoad);
            individualFutureLoadMap.remove(individualWithHighestFutureLoad);
            individualFutureLoadMap.put(individualWithHighestFutureLoad, assignedLoad + loadDifference);
        } else if (loadDifference > 0) {
            //This means that the algorithm assigned less load than the totalInflow
            Long assignedLoad = individualFutureLoadMap.get(individualWithLowestFutureLoad);
            individualFutureLoadMap.remove(individualWithLowestFutureLoad);
            individualFutureLoadMap.put(individualWithLowestFutureLoad, assignedLoad + loadDifference);
        }

        return new ccaLoadBalancingEntity(individualFutureLoadMap, individualWithLowestFutureLoad);
    }
}