public class ccaJobManagerAnalyze implements ccaIJobManager {

    public static String getTeamUserIds(String teamName, String groupName) {
        String userIdsForQuery;
        try {
            for (String userId : ccaJobContext.getInstance()
                    .getUsersStructure().getTeams().get(teamName).getGroupByName(groupName).getAllGroupMembersIds()) {
                if (String.isEmpty(userIdsForQuery)) {
                    userIdsForQuery = '\'' + userId + '\'';
                } else {
                    userIdsForQuery += ',\'' + userId + '\'';
                }
            }
        } catch (Exception e) {
            ccaJobContext.getInstance().getLogger().logEvent('ccaJobManagerAnalyze - getTeamUserIds - Exception: ' + e.getMessage());
        }
        if (String.isEmpty(userIdsForQuery)) {
            userIdsForQuery = '\'\'';
        }
        return userIdsForQuery;
    }

    public static String getAnalyzeQuery() {

        return 'SELECT Id, Overdue_Days__c, DMC__c, Collection_Case_Owner__c,Product_Name__c, name, loan__loan_status__c'
                + ' FROM loan__loan_account__c'
                + ' WHERE'

                //Ignore the following contract status
                + ' loan__Loan_Status__c NOT IN (\'Active - Marked for Closure\', \'Closed - Obligations met\', \'Canceled\')'
                + ' AND ('
                + ' ('

                //DMC contracts conditions
                + ' DMC__c != null'
                + ' AND ('
                //Get Default contracts which are not already assigned to DMC_Default team
                + ' (loan__loan_status__c IN (\'Closed - Settled Off\', \'Closed - Sold Off\') AND Collection_Case_Owner__c NOT IN (' + getTeamUserIds('ccaDMC', 'ccaDMCDefault') + '))'
                //All these will be assigned to non Default teams
                + ' OR (loan__loan_status__c IN (\'Closed- Written Off\', \'Active - Bad Standing\', \'Active - Good Standing\'))'
                + ' )'
                + ' )'
                + ' OR ('

                //Non DMC contracts conditions
                + ' DMC__c = null'
                + ' AND('
                //Cured - Assign to HAL
                + ' (Overdue_Days__c = 0 AND Collection_Case_Owner__r.name != \'HAL\' AND Collection_Case_Owner__c != \'\' AND Collection_Case_Owner__c != null)'
                //All these are assigned to non Default teams
                + ' OR (Overdue_Days__c >= 1 AND Overdue_Days__c <= 180 AND loan__loan_status__c IN (\'Active - Bad Standing\', \'Active - Good Standing\'))'
                //Get Default contracts which are not already assigned to DTC Default
                + ' OR (Product_Name__c != \'Point Of Need\' AND Overdue_Days__c >= 1 AND loan__loan_status__c NOT IN (\'Active - Bad Standing\', \'Active - Good Standing\') AND Collection_Case_Owner__c NOT IN (' + getTeamUserIds('ccaDTC', 'ccaDTCDefault') + '))'
                //Get Default contracts which are not already assigned to PON Default
                + ' OR (Product_Name__c = \'Point Of Need\' AND Overdue_Days__c >= 1 AND loan__loan_status__c NOT IN (\'Active - Bad Standing\', \'Active - Good Standing\') AND Collection_Case_Owner__c NOT IN (' + getTeamUserIds('ccaPON', 'ccaPONDefault') + '))'
                + ' )'
                + ' )'
                + ' )';
    }

    public static Database.QueryLocator start(Database.BatchableContext BC, ccaJob job) {

        //Set up the working context (ccaJobSummaryReport, ccaUsersStructure, ccaLogger, custom setting) for this and all the chained jobs
        //It MUST be done here because the main ccaJob constructor methods are not executed when batches are called from the scheduled jobs
        job.initJobContext();

        //Get the ANALYZE query for the job
        String query = getAnalyzeQuery();

        //Log an event with the query that will be used for the ANALYZE job
        ccaJobContext.getInstance().getLogger().logEvent('ccaJobManagerAnalyze - start - Query: ' + query);

        //Return the QueryLocator
        return Database.getQueryLocator(query);
    }

    public static void execute(Database.BatchableContext BC, List<Object> scope, ccaJob job) {

        //Share the working context with each batch and make the same instance of the working context available for every class
        ccaJobContext.shareInstance(job.jobContext);

        //Get the corresponding cases for the loan__loan_account__c records in the scope
        Map<Id, Case> casesMap = new Map<Id, Case>();
        for (Case aCase : [
                SELECT Id, Owner.Id, CL_Contract__c, PreviousOwner__c, PreviousOwner__r.name, Owner.Name
                FROM Case
                WHERE CL_Contract__c IN :(List<loan__loan_account__c>) scope
                AND RecordTypeId = :Label.RecordType_Collections_Id
                ORDER BY Id DESC
        ]) {
            if (!casesMap.keySet().contains(aCase.CL_Contract__c)) {
                casesMap.put(aCase.CL_Contract__c, aCase);
            }
        }

        //Get the corresponding last payment for the loan__loan_account__c records in the scope
        Map<Id, loan__Loan_Payment_Transaction__c> paymentsMap = new Map<Id, loan__Loan_Payment_Transaction__c>();
        for (loan__Loan_Payment_Transaction__c payment : [
                SELECT id, loan__Reversal_Reason__c, loan__Loan_Account__c
                FROM loan__Loan_Payment_Transaction__c
                WHERE loan__loan_account__c in :(List<loan__loan_account__c>) scope
                ORDER BY loan__transaction_date__c DESC
        ]) {
            if (!paymentsMap.containsKey(payment.loan__Loan_Account__c)) {
                paymentsMap.put(payment.loan__Loan_Account__c, payment);
            }
        }

        //Analyze scenario and save it to the log
        ccaScenario scenario;
        List<ccaLog__c> logList = new List<ccaLog__c>();
        for (Object contract : scope) {
            scenario = new ccaScenario(//Build the scenario with the corresponding contract, case and last payment.
                    (loan__loan_account__c) contract
                    , (Case) casesMap.get(((loan__loan_account__c) contract).Id)
                    , (loan__Loan_Payment_Transaction__c) paymentsMap.get(((loan__loan_account__c) contract).Id)
            );

            //Analyze the scenario according to the corresponding Business Logic for its type and add the ANALYZE batch number
            logList.add(ccaScenarioAnalyzerFactory.getScenarioAnalyzer(scenario.type).analyze(scenario).toCCALog(job.batchNumber));
            //ccaJobContext.getInstance().getJobSummaryReport().countAnalyzedScenario(scenario);
        }
        if (!logList.isEmpty()) {
            try {
                Database.SaveResult[] saveResultsList = Database.insert(logList, false);
                for (Integer i = 0; i < saveResultsList.size(); i++) {
                    if (!saveResultsList[i].isSuccess()) {
                        job.getJobContext().getLogger().logEvent('ccaJobManagerAnalyze - insert logList - ' + JSON.serialize(logList[i]) + ' - ' + saveResultsList[i].getErrors());
                    }
                }
            } catch (Exception e) {
                job.getJobContext().getLogger().logEvent('ANALYZE batch number: ' + job.batchNumber + ', Exception:' + e.getMessage());
            }
        }
    }

    public static void execute(SchedulableContext sc, ccaJob job) {
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().ANALYZE_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().ANALYZE_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().ANALYZE_scope__c.intValue()
                        : 350
        );
    }

    public static void finish(Database.BatchableContext BC, ccaJob job) {

        //Set the jobAction for the next job and execute it
        job.jobAction = ccaUtil.JOB_ACTION_ASSIGN;
        database.executebatch(
                job
                , ccaJobContext.getInstance().getCustomSetting().ASSIGN_scope__c != null
                        && ccaJobContext.getInstance().getCustomSetting().ASSIGN_scope__c.intValue() > 0
                        ? ccaJobContext.getInstance().getCustomSetting().ASSIGN_scope__c.intValue()
                        : 100
        );
    }
}