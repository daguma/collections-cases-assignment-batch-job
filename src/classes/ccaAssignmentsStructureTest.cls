@isTest
public class ccaAssignmentsStructureTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    static testMethod void validate_assignments_structure_entity() {
        Test.startTest();
        Id userId = [SELECT Id FROM User LIMIT 1].Id;
        Map<String, ccaLoadBalancingEntity> map1 = new Map<String, ccaLoadBalancingEntity>{
                'Test' => new ccaLoadBalancingEntity(new Map<Id, Long>{
                        userId => 1
                }, userId)
        };
        Map<String, Map<Id, Integer>> map2 = new Map<String, Map<Id, Integer>>{
                'Test2' => new Map<Id, Integer>{
                        userId => 1
                }
        };
        Map<String, Integer> map3 = new Map<String, Integer>{
                'Test3' => 1
        };
        ccaAssignmentsStructure.AssignmentsStructureEntity testASE = new ccaAssignmentsStructure.AssignmentsStructureEntity(map1, map2, map3);
        system.assertEquals(map1, testASE.loadBalancedMap);
        system.assertEquals(map2, testASE.assignedPerGroupBeforeProcessMap);
        system.assertEquals(map3, testASE.totalInflowCasesToAssignPerGroupMap);
        Test.stopTest();
    }

    static testMethod void validate_constructor() {
        Test.startTest();
        ccaAssignmentsStructure ccaAS = new ccaAssignmentsStructure(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values());
        system.assertNotEquals(null, ccaAS);
        system.assertNotEquals(null, ccaAS.loadBalancedMap);
        system.assertNotEquals(null, ccaAS.assignedPerGroupBeforeProcessMap);
        system.assertNotEquals(null, ccaAS.totalInflowCasesToAssignPerGroupMap);
        system.assertNotEquals(null, ccaAS.initialAssignmentsStructure);
        Test.stopTest();
    }

    static testMethod void validate_get_assignments_structure() {
        Test.startTest();
        ccaAssignmentsStructure.AssignmentsStructureEntity testASE =
                ccaAssignmentsStructure.getAssignmentsStructure(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values());
        system.assertNotEquals(null, testASE);
        system.assertNotEquals(null, testASE.loadBalancedMap);
        system.assertNotEquals(null, testASE.assignedPerGroupBeforeProcessMap);
        system.assertNotEquals(null, testASE.totalInflowCasesToAssignPerGroupMap);
        Test.stopTest();
    }

    static testMethod void validate_get_initial_assignments_structure() {
        Test.startTest();
        ccaAssignmentsStructure testAS = new ccaAssignmentsStructure(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values());
        system.assertNotEquals(null, testAS.getInitialAssignmentsStructure());
        Test.stopTest();
    }

    static testMethod void validate_get_next_assignee_id() {
        Test.startTest();
        ccaAssignmentsStructure testAS = new ccaAssignmentsStructure(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values());
        system.debug(testAS.getInitialAssignmentsStructure());
        system.assertEquals(null, testAS.getNextAssigneeId(null));
        system.assertEquals(null, testAS.getNextAssigneeId(''));
        system.assertEquals(null, testAS.getNextAssigneeId('ccaDMC1'));
        Test.stopTest();
    }

}