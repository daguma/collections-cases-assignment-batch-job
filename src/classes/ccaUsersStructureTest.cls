@isTest
public class ccaUsersStructureTest {

    @testSetup
    static void test_data() {
        ccaCustomSetting__c cuSettings = new ccaCustomSetting__c(
                name = 'cca',
                Main_public_group__c = 'CollectionsCasesAssignment',
                Audit_public_group__c = 'CollectionsCasesAssignmentAudit',
                Update_Cases__c = true,
                Update_Contracts__c = true
        );
        insert cuSettings;
    }

    @isTest static void validate_constructor() {
        Test.startTest();
        system.assert(
                ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).teamsMap != null
                , 'ccaUsersStructure is null'
        );
        system.assert(
                !ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).teamsMap.isEmpty()
                , 'ccaUsersStructure has no teams'
        );
        Test.stopTest();
    }

    @isTest static void validate_set_up_teams_exception() {
        Test.startTest();
        try {
            Map<String, ccaUsersStructure.Team> testMap = ccaUsersStructure.setUpTeams('CollectionsCasesAssignment1');
        } catch (ccaUsersStructure.ccaUsersStructureException e) {
            system.assert(true, 'The ccaUsersStructure.ccaUsersStructureException was thrown and caught.');
        }

        try {
            Map<String, ccaUsersStructure.Team> testMap = ccaUsersStructure.setUpTeams('');
        } catch (ccaUsersStructure.ccaUsersStructureException e) {
            system.assert(true, 'The ccaUsersStructure.ccaUsersStructureException was thrown and caught.');
        }

        try {
            Map<String, ccaUsersStructure.Team> testMap = ccaUsersStructure.setUpTeams(null);
        } catch (ccaUsersStructure.ccaUsersStructureException e) {
            system.assert(true, 'The ccaUsersStructure.ccaUsersStructureException was thrown and caught.');
        }

        try {
            Map<String, ccaUsersStructure.Team> testMap = ccaUsersStructure.setUpTeams('CollectionsCasesAssignment');
        } catch (ccaUsersStructure.ccaUsersStructureException e) {
            system.assert(false, 'The ccaUsersStructure.ccaUsersStructureException was thrown by ccaUsersStructure.setUpTeams()');
        }
        system.assert(true, 'The ccaUsersStructure.ccaUsersStructureException was not thrown by ccaUsersStructure.setUpTeams().');
        Test.stopTest();
    }

    @isTest static void validate_set_up_teams() {
        Test.startTest();
        Map<String, ccaUsersStructure.Team> testMap = ccaUsersStructure.setUpTeams('CollectionsCasesAssignment');
        system.assert(testMap != null, 'testMap is null');
        system.assert(!testMap.isEmpty(), 'testMap has no teams');
        Test.stopTest();
    }

    @isTest static void validate_get_teams() {
        Test.startTest();
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams() != null, 'Users Structure teams is null');
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().isEmpty(), 'Users Structure teams is empty');
        Test.stopTest();
    }

    @isTest static void validate_get_all_groups_list() {
        Test.startTest();
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values()[0].getAllGroupsList() != null, 'Team has no groups');
        Test.stopTest();
    }

    @isTest static void validate_get_group_name() {
        Test.startTest();
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values()[0].getAllGroupsList()[0].getGroupName() != null, 'Group has no name!!!');
        Test.stopTest();
    }

    @isTest static void validate_get_all_group_members_ids() {
        Test.startTest();
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values()[0].getAllGroupsList()[0].getAllGroupMembersIds() != null, 'Group users list is null');
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getTeams().values()[0].getAllGroupsList()[0].getAllGroupMembersIds().isEmpty(), 'Group users list is empty');
        Test.stopTest();
    }

    @isTest static void validate_check_user_belongs_in_dpd_level() {
        Test.startTest();
        Group ccaDTC1 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Map<Id, AggregateResult> collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC1.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        Id collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('', 1, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel(null, 1, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', null, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 1, null));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 1, collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 15, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 16, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 61, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 60, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 180, collectorUserId));

        Group ccaDTC2 = [SELECT Id FROM Group WHERE developername = 'DTC_16_60'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC2.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 1, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 15, collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 16, collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 60, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 61, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 180, collectorUserId));

        Group ccaDTC3 = [SELECT Id FROM Group WHERE developername = 'DTC_61_180'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC3.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 1, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 15, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 16, collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 60, collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 61, collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 180, collectorUserId));


        User notCollector = [SELECT Id FROM User WHERE Name = 'James T Holder'];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 1, notCollector.Id));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 15, notCollector.Id));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 16, notCollector.Id));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 60, notCollector.Id));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 61, notCollector.Id));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInDpdLevel('ccaDTC', 180, notCollector.Id));

        Test.stopTest();
    }

    @isTest static void validate_get_group_name_by_dpd() {
        Test.startTest();
        Group ccaDTC1 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Map<Id, AggregateResult> collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC1.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        Id collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assertEquals('ccaDTC1', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 1));
        system.assertEquals('ccaDTC1', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 15));
        system.assertNotEquals('ccaDTC1', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 16));
        system.assertNotEquals('ccaDTC1', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 60));
        system.assertNotEquals('ccaDTC1', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 61));
        system.assertNotEquals('ccaDTC1', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 180));
        Group ccaDTC2 = [SELECT Id FROM Group WHERE developername = 'DTC_16_60'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC2.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assertNotEquals('ccaDTC2', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 1));
        system.assertNotEquals('ccaDTC2', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 15));
        system.assertEquals('ccaDTC2', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 16));
        system.assertEquals('ccaDTC2', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 60));
        system.assertNotEquals('ccaDTC2', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 61));
        system.assertNotEquals('ccaDTC2', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 180));

        Group ccaDTC3 = [SELECT Id FROM Group WHERE developername = 'DTC_61_180'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC3.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assertNotEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 1));
        system.assertNotEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 15));
        system.assertNotEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 16));
        system.assertNotEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 60));
        system.assertEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 61));
        system.assertEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 180));
        system.assertEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 181));
        system.assertEquals('ccaDTC3', ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).getGroupNameByDpd('ccaDTC', 1800000));
        Test.stopTest();
    }

    @isTest static void validate_check_user_belongs_in_group() {
        Test.startTest();
        Group ccaDTC1 = [SELECT Id FROM Group WHERE developername = 'DTC_1_15'];
        Map<Id, AggregateResult> collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC1.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        Id collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('', 'ccaDTC1', collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC1', collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC2', collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC3', collectorUserId));
        Group ccaDTC2 = [SELECT Id FROM Group WHERE developername = 'DTC_16_60'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC2.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC1', collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC2', collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC3', collectorUserId));
        Group ccaDTC3 = [SELECT Id FROM Group WHERE developername = 'DTC_61_180'];
        collectorsMap = new Map<Id, AggregateResult>([
                SELECT UserOrGroupId Id, GroupId
                FROM GroupMember
                WHERE GroupId = :ccaDTC3.Id
                GROUP BY UserOrGroupId, GroupId
        ]);
        collectorUserId = (new List<Id>(collectorsMap.keySet()))[0];
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC1', collectorUserId));
        system.assert(!ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC2', collectorUserId));
        system.assert(ccaUsersStructure.getInstance(ccaCustomSetting__c.getInstance('cca').Main_public_group__c).checkUserBelongsInGroup('ccaDTC', 'ccaDTC3', collectorUserId));
        Test.stopTest();
    }

}